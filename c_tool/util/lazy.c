#include <stdio.h>
int main()
{
    int i;
    /*
    for(i = 0;i < 64;++i)
    {
        //printf("%d: chunk_buffer[%d:%d] <= input_byte;\n", i, 8*i + 7, 8 * i);
        //printf("%d: k = K%d;\n", i, i);
        //printf("reg [31:0] m%d;\n", i);
        //printf("%d: m = m%d;\n", i, i);
    }
    */
    /*
    for(i = 0;i < 64;i = i + 4)
    {
        printf("m%d = {chunk_data[%d:%d], chunk_data[%d:%d], chunk_data[%d:%d], chunk_data[%d:%d]};\n", i/4, 8*i+7, 8*i, 8*(i+1)+7, 8*(i+1), 8*(i+2)+7, 8*(i+2), 8*(i+3)+7, 8*(i+3));
    }
    */
    /*
    for(i = 16;i < 64;++i)
    {
        printf("m%d = (({m%d[16:0],m%d[31:17]}) ^ ({m%d[18:0],m%d[31:19]}) ^ (m%d >> 10)) + m%d + (({m%d[6:0],m%d[31:7]}) ^ ({m%d[17:0],m%d[31:18]}) ^ (m%d >> 3)) + m%d;\n", i, i-2, i-2, i-2, i-2, i-2, i-7, i-15, i-15, i-15, i-15, i-15, i-16);
    }
    */
    /*
    for(i = 0;i < 64;++i)
    {
        printf(".m%d(m%d), ",i ,i);
        //printf("output reg [31:0] m%d;\n", i);
    }
    */
    /*
    for(i = 0;i < 256;i += 8)
    {
        printf("sha_stage1_data[%d:%d], ", i + 7, i);
    }
    */
    /*
    for(i = 0;i < 32;++i)
    {
        printf("%d: tx_byte = hash_result[%d:%d];\n", i, 8 * i + 7, 8 * i);
    }
    */
    /*
    for(i = 0;i < 64;++i)
    {
        printf("wire [31:0] m%d;\n", i);
    }
    */
    /*
    for(i = 0;i < 64;++i)
    {
        printf("m%d, ", i);
    }
    */
    /*
    for(i = 0;i < 48;++i)
    {
        printf("%d: m_op_a = m%d;\n", i, i + 14);
    }
    for(i = 0;i < 48;++i)
    {
        printf("%d: m_op_b = m%d;\n", i, i + 9);
    }
    for(i = 0;i < 48;++i)
    {
        printf("%d: m_op_c = m%d;\n", i, i + 1);
    }
    for(i = 0;i < 48;++i)
    {
        printf("%d: m_op_d = m%d;\n", i, i);
    }
    */
    for(i = 0;i < 48;++i)
    {
        printf("    always @ (posedge clk) begin\n");
        printf("        if(rst | clear)\n");
        printf("            m%d <= 0;\n", i + 16);
        printf("        else if(loop_counter == %d)\n", i);
        printf("            m%d <= m_rslt;\n", i + 16);
        printf("        else\n");
        printf("            m%d <= m%d;\n", i + 16, i + 16);
        printf("    end\n");
    }
    return 0;
}
