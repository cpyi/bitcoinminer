module input_interface(clk, rst, input_byte, input_valid, input_last, input_ready, chunk_data, chunk_ready, chunk_last, chunk_ack);
    input clk;
    input rst;
    input [7:0] input_byte;
    input input_valid;
    input input_last;
    output reg input_ready;
    output reg [511:0] chunk_data;
    output reg chunk_last;
    output reg chunk_ready;
    input chunk_ack;
    reg [511:0] chunk_buffer;
    reg [2:0] current_state;
    reg [2:0] next_state;
    reg [5:0] chunk_pointer;
    reg [5:0] chunk_pointer_next;
    reg [63:0] size;
    reg [63:0] size_next;
    reg [511:0] debug;
    parameter S_INIT = 3'd0;
    parameter S_ACCU = 3'd1;
    parameter S_FULL = 3'd3;
    parameter S_FILL0 = 3'd4;
    parameter S_FILL1 = 3'd5;
    parameter S_FILL2 = 3'd6;
    always @ (*) begin
        case(current_state)
            S_INIT:
                next_state = S_ACCU;
            S_ACCU: begin
                if(input_valid && !input_last && chunk_pointer <= 6'd62) 
                    next_state = S_ACCU;
                else if(input_valid && !input_last && chunk_pointer == 6'd63)
                    next_state = S_FULL;
                else if(input_valid && input_last && chunk_pointer <= 6'd54)
                    next_state = S_FILL0;
                else if(input_valid && input_last && chunk_pointer >= 6'd55)
                    next_state = S_FILL1;
                else
                    next_state = S_ACCU;
            end
            S_FULL: begin
                if(chunk_ack)
                    next_state = S_INIT;
                else
                    next_state = S_FULL;
            end
            S_FILL0: begin
                if(chunk_ack)
                    next_state = S_INIT;
                else
                    next_state = S_FILL0;
            end
            S_FILL1: begin
                if(chunk_ack)
                    next_state = S_FILL2;
                else
                    next_state = S_FILL1;
            end
            S_FILL2: begin
                if(chunk_ack)
                    next_state = S_INIT;
                else
                    next_state = S_FILL2;
            end
        endcase
    end

    //size
    always @ (*) begin
        if((chunk_ack == 1) && ((current_state == S_FILL0) || (current_state == S_FILL2)))
            size_next = 64'b0;
        else if(input_valid && current_state == S_ACCU)
            size_next = size + 8;
        else
            size_next = size;
    end

    //chunk_pointer
    always @(*) begin
        if(current_state == S_INIT)
            chunk_pointer_next = 6'b0;
        else if(input_valid && current_state == S_ACCU)
            chunk_pointer_next = chunk_pointer + 1;
        else
            chunk_pointer_next = chunk_pointer;
    end

    always @ (posedge clk) begin
        if(rst) begin
            chunk_pointer <= 6'b0;
            size <= 64'b0;
            current_state <= S_INIT;
            chunk_buffer <= 512'b0;
        end
        else begin
            current_state <= next_state;
            size <= size_next;
            chunk_pointer <= chunk_pointer_next;
            if(chunk_ack && (current_state == S_FULL || current_state == S_FILL0 || current_state == S_FILL2)) begin
                chunk_buffer <= 512'b0;
            end
            if(input_valid && current_state == S_ACCU) begin
                case(chunk_pointer)
                    0: chunk_buffer[7:0] <= input_byte;
                    1: chunk_buffer[15:8] <= input_byte;
                    2: chunk_buffer[23:16] <= input_byte;
                    3: chunk_buffer[31:24] <= input_byte;
                    4: chunk_buffer[39:32] <= input_byte;
                    5: chunk_buffer[47:40] <= input_byte;
                    6: chunk_buffer[55:48] <= input_byte;
                    7: chunk_buffer[63:56] <= input_byte;
                    8: chunk_buffer[71:64] <= input_byte;
                    9: chunk_buffer[79:72] <= input_byte;
                    10: chunk_buffer[87:80] <= input_byte;
                    11: chunk_buffer[95:88] <= input_byte;
                    12: chunk_buffer[103:96] <= input_byte;
                    13: chunk_buffer[111:104] <= input_byte;
                    14: chunk_buffer[119:112] <= input_byte;
                    15: chunk_buffer[127:120] <= input_byte;
                    16: chunk_buffer[135:128] <= input_byte;
                    17: chunk_buffer[143:136] <= input_byte;
                    18: chunk_buffer[151:144] <= input_byte;
                    19: chunk_buffer[159:152] <= input_byte;
                    20: chunk_buffer[167:160] <= input_byte;
                    21: chunk_buffer[175:168] <= input_byte;
                    22: chunk_buffer[183:176] <= input_byte;
                    23: chunk_buffer[191:184] <= input_byte;
                    24: chunk_buffer[199:192] <= input_byte;
                    25: chunk_buffer[207:200] <= input_byte;
                    26: chunk_buffer[215:208] <= input_byte;
                    27: chunk_buffer[223:216] <= input_byte;
                    28: chunk_buffer[231:224] <= input_byte;
                    29: chunk_buffer[239:232] <= input_byte;
                    30: chunk_buffer[247:240] <= input_byte;
                    31: chunk_buffer[255:248] <= input_byte;
                    32: chunk_buffer[263:256] <= input_byte;
                    33: chunk_buffer[271:264] <= input_byte;
                    34: chunk_buffer[279:272] <= input_byte;
                    35: chunk_buffer[287:280] <= input_byte;
                    36: chunk_buffer[295:288] <= input_byte;
                    37: chunk_buffer[303:296] <= input_byte;
                    38: chunk_buffer[311:304] <= input_byte;
                    39: chunk_buffer[319:312] <= input_byte;
                    40: chunk_buffer[327:320] <= input_byte;
                    41: chunk_buffer[335:328] <= input_byte;
                    42: chunk_buffer[343:336] <= input_byte;
                    43: chunk_buffer[351:344] <= input_byte;
                    44: chunk_buffer[359:352] <= input_byte;
                    45: chunk_buffer[367:360] <= input_byte;
                    46: chunk_buffer[375:368] <= input_byte;
                    47: chunk_buffer[383:376] <= input_byte;
                    48: chunk_buffer[391:384] <= input_byte;
                    49: chunk_buffer[399:392] <= input_byte;
                    50: chunk_buffer[407:400] <= input_byte;
                    51: chunk_buffer[415:408] <= input_byte;
                    52: chunk_buffer[423:416] <= input_byte;
                    53: chunk_buffer[431:424] <= input_byte;
                    54: chunk_buffer[439:432] <= input_byte;
                    55: chunk_buffer[447:440] <= input_byte;
                    56: chunk_buffer[455:448] <= input_byte;
                    57: chunk_buffer[463:456] <= input_byte;
                    58: chunk_buffer[471:464] <= input_byte;
                    59: chunk_buffer[479:472] <= input_byte;
                    60: chunk_buffer[487:480] <= input_byte;
                    61: chunk_buffer[495:488] <= input_byte;
                    62: chunk_buffer[503:496] <= input_byte;
                    63: chunk_buffer[511:504] <= input_byte;
                endcase
            end
        end
    end
    always @ (*) begin
        case(current_state)
            S_INIT: begin
                input_ready = 1'b0;
                chunk_data = 512'b0;
                chunk_last = 1'b0;
                chunk_ready = 1'b0;
            end
            S_ACCU: begin
                input_ready = 1'b1;
                chunk_data = 512'b0;
                chunk_last = 1'b0;
                chunk_ready = 1'b0;
            end
            S_FULL: begin
                input_ready = 1'b0;
                chunk_data = chunk_buffer;
                chunk_last = 1'b0;
                chunk_ready = 1'b1;
            end
            S_FILL0: begin
                input_ready = 1'b0;
                chunk_data = chunk_buffer ^ (8'h80 << ({3'b0, chunk_pointer} << 3)) ^ {size[7:0], 504'b0} ^ {8'b0, size[15:8] , 496'b0} ^ {16'b0, size[23:16], 488'b0} ^ {24'b0, size[31:24], 480'b0} ^ {24'b0, size[39:32], 472'b0} ^ {32'b0, size[47:40], 464'b0} ^ {40'b0, size[47:40], 464'b0} ^ {48'b0, size[55:48], 456'b0} ^ {56'b0, size[63:56], 448'b0};
                debug = (1 << ({3'b0, chunk_pointer} << 3));
                chunk_last = 1'b1;
                chunk_ready = 1'b1;
            end
            S_FILL1: begin
                input_ready = 1'b0;
                if(chunk_pointer == 6'd0)
                    chunk_data = chunk_buffer;
                else
                    chunk_data = chunk_buffer ^ (8'h80 << ({3'b0, chunk_pointer} << 3));
                chunk_last = 1'b0;
                chunk_ready = 1'b1;
            end
            S_FILL2: begin
                input_ready = 1'b0;
                if(chunk_pointer == 6'd0)
                    chunk_data = {size[7:0], 504'b0} ^ {8'b0, size[15:8] , 496'b0} ^ {16'b0, size[23:16], 488'b0} ^ {24'b0, size[31:24], 480'b0} ^ {24'b0, size[39:32], 472'b0} ^ {32'b0, size[47:40], 464'b0} ^ {40'b0, size[47:40], 464'b0} ^ {48'b0, size[55:48], 456'b0} ^ {56'b0, size[63:56], 448'b0} ^ {448'b0, 8'h80};
                else
                    chunk_data = {size[7:0], 504'b0} ^ {8'b0, size[15:8] , 496'b0} ^ {16'b0, size[23:16], 488'b0} ^ {24'b0, size[31:24], 480'b0} ^ {24'b0, size[39:32], 472'b0} ^ {32'b0, size[47:40], 464'b0} ^ {40'b0, size[47:40], 464'b0} ^ {48'b0, size[55:48], 456'b0} ^ {56'b0, size[63:56], 448'b0};
                chunk_last = 1'b1;
                chunk_ready = 1'b1;
            end
        endcase
    end
endmodule
