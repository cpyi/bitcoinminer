`timescale 1ns / 1ps
/*
    Basic Desc:
        SHA256 double hash calculation engine dedicated for bitcoin double hash.
        Three main stage for first hash(2 stage) and second hash(1 stage)
    IO signals:
        clk: posedge clock signal
        rst: active high sync reset
        chunk1: first chunk for first hash
        chunk2: second chunk for first hash
        output_valid: indicate output_data is ready
        output_data: SHA256 double hash result.
    Author:
        cpyi
    To do:
        k parameter can be share by three stages.
   */
module sha256_pipe(clk, rst, pipe_init, chunk1, chunk2, output_valid, output_data);

    //Number of pipeline stages
    localparam STG_NUM = 16;

    //Number of loop per stage, should be 64 / STG_NUM
    localparam STG_LOOP = 4;

    //Log of number of loop per stage, should be lg(STG_LOOP)
    localparam STG_LOOP_LOG = 2;
    
    //Input signals
    input clk, rst, pipe_init;
    input [511:0] chunk1;
    input [511:0] chunk2;

    //Output signals
    output output_valid;
    output [255:0] output_data;

    reg [STG_LOOP_LOG - 1:0] feedback_counter;
    wire feedback_pass;
    wire feedback_reset;
    reg [5:0] buffer_counter;
    wire buffer_pass;
    wire buffer_reset;
    reg [7:0] init_counter;
    reg pipeout_ready;
    
    //Paramter k, shared by three stages
    wire [31:0] k_constant[STG_NUM - 1:0];

    //Pipelined a to h register
    wire [31:0] pipe_reg_a[STG_NUM * 3 - 1 : 0];
    wire [31:0] pipe_reg_b[STG_NUM * 3 - 1 : 0];
    wire [31:0] pipe_reg_c[STG_NUM * 3 - 1 : 0];
    wire [31:0] pipe_reg_d[STG_NUM * 3 - 1 : 0];
    wire [31:0] pipe_reg_e[STG_NUM * 3 - 1 : 0];
    wire [31:0] pipe_reg_f[STG_NUM * 3 - 1 : 0];
    wire [31:0] pipe_reg_g[STG_NUM * 3 - 1 : 0];
    wire [31:0] pipe_reg_h[STG_NUM * 3 - 1 : 0];

    wire [31:0] a_out;
    wire [31:0] b_out;
    wire [31:0] c_out;
    wire [31:0] d_out;
    wire [31:0] e_out;
    wire [31:0] f_out;
    wire [31:0] g_out;
    wire [31:0] h_out;

    //Original m parameter from the chunk data
    wire [31:0] m0_chk_1;
    wire [31:0] m1_chk_1;
    wire [31:0] m2_chk_1;
    wire [31:0] m3_chk_1;
    wire [31:0] m4_chk_1;
    wire [31:0] m5_chk_1;
    wire [31:0] m6_chk_1;
    wire [31:0] m7_chk_1;
    wire [31:0] m8_chk_1;
    wire [31:0] m9_chk_1;
    wire [31:0] m10_chk_1;
    wire [31:0] m11_chk_1;
    wire [31:0] m12_chk_1;
    wire [31:0] m13_chk_1;
    wire [31:0] m14_chk_1;
    wire [31:0] m15_chk_1;

    wire [31:0] m0_chk_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m1_chk_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m2_chk_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m3_chk_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m4_chk_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m5_chk_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m6_chk_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m7_chk_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m8_chk_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m9_chk_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m10_chk_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m11_chk_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m12_chk_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m13_chk_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m14_chk_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m15_chk_1_reg[STG_NUM - 1 : 0];

    wire [31:0] m0_chk_1_out[STG_NUM - 1 : 0];
    wire [31:0] m1_chk_1_out[STG_NUM - 1 : 0];
    wire [31:0] m2_chk_1_out[STG_NUM - 1 : 0];
    wire [31:0] m3_chk_1_out[STG_NUM - 1 : 0];
    wire [31:0] m4_chk_1_out[STG_NUM - 1 : 0];
    wire [31:0] m5_chk_1_out[STG_NUM - 1 : 0];
    wire [31:0] m6_chk_1_out[STG_NUM - 1 : 0];
    wire [31:0] m7_chk_1_out[STG_NUM - 1 : 0];
    wire [31:0] m8_chk_1_out[STG_NUM - 1 : 0];
    wire [31:0] m9_chk_1_out[STG_NUM - 1 : 0];
    wire [31:0] m10_chk_1_out[STG_NUM - 1 : 0];
    wire [31:0] m11_chk_1_out[STG_NUM - 1 : 0];
    wire [31:0] m12_chk_1_out[STG_NUM - 1 : 0];
    wire [31:0] m13_chk_1_out[STG_NUM - 1 : 0];
    wire [31:0] m14_chk_1_out[STG_NUM - 1 : 0];
    wire [31:0] m15_chk_1_out[STG_NUM - 1 : 0];

    wire [31:0] m0_chk_2;
    wire [31:0] m1_chk_2;
    wire [31:0] m2_chk_2;
    wire [31:0] m3_chk_2;
    wire [31:0] m4_chk_2;
    wire [31:0] m5_chk_2;
    wire [31:0] m6_chk_2;
    wire [31:0] m7_chk_2;
    wire [31:0] m8_chk_2;
    wire [31:0] m9_chk_2;
    wire [31:0] m10_chk_2;
    wire [31:0] m11_chk_2;
    wire [31:0] m12_chk_2;
    wire [31:0] m13_chk_2;
    wire [31:0] m14_chk_2;
    wire [31:0] m15_chk_2;

    wire [31:0] m0_chk_2_reg[STG_NUM * 2 - 1 : 0];
    wire [31:0] m1_chk_2_reg[STG_NUM * 2 - 1 : 0];
    wire [31:0] m2_chk_2_reg[STG_NUM * 2 - 1 : 0];
    wire [31:0] m3_chk_2_reg[STG_NUM * 2 - 1 : 0];
    wire [31:0] m4_chk_2_reg[STG_NUM * 2 - 1 : 0];
    wire [31:0] m5_chk_2_reg[STG_NUM * 2 - 1 : 0];
    wire [31:0] m6_chk_2_reg[STG_NUM * 2 - 1 : 0];
    wire [31:0] m7_chk_2_reg[STG_NUM * 2 - 1 : 0];
    wire [31:0] m8_chk_2_reg[STG_NUM * 2 - 1 : 0];
    wire [31:0] m9_chk_2_reg[STG_NUM * 2 - 1 : 0];
    wire [31:0] m10_chk_2_reg[STG_NUM * 2 - 1 : 0];
    wire [31:0] m11_chk_2_reg[STG_NUM * 2 - 1 : 0];
    wire [31:0] m12_chk_2_reg[STG_NUM * 2 - 1 : 0];
    wire [31:0] m13_chk_2_reg[STG_NUM * 2 - 1 : 0];
    wire [31:0] m14_chk_2_reg[STG_NUM * 2 - 1 : 0];
    wire [31:0] m15_chk_2_reg[STG_NUM * 2 - 1 : 0];

    wire [31:0] m0_chk_2_out[STG_NUM * 2 - 1 : 0];
    wire [31:0] m1_chk_2_out[STG_NUM * 2 - 1 : 0];
    wire [31:0] m2_chk_2_out[STG_NUM * 2 - 1 : 0];
    wire [31:0] m3_chk_2_out[STG_NUM * 2 - 1 : 0];
    wire [31:0] m4_chk_2_out[STG_NUM * 2 - 1 : 0];
    wire [31:0] m5_chk_2_out[STG_NUM * 2 - 1 : 0];
    wire [31:0] m6_chk_2_out[STG_NUM * 2 - 1 : 0];
    wire [31:0] m7_chk_2_out[STG_NUM * 2 - 1 : 0];
    wire [31:0] m8_chk_2_out[STG_NUM * 2 - 1 : 0];
    wire [31:0] m9_chk_2_out[STG_NUM * 2 - 1 : 0];
    wire [31:0] m10_chk_2_out[STG_NUM * 2 - 1 : 0];
    wire [31:0] m11_chk_2_out[STG_NUM * 2 - 1 : 0];
    wire [31:0] m12_chk_2_out[STG_NUM * 2 - 1 : 0];
    wire [31:0] m13_chk_2_out[STG_NUM * 2 - 1 : 0];
    wire [31:0] m14_chk_2_out[STG_NUM * 2 - 1 : 0];
    wire [31:0] m15_chk_2_out[STG_NUM * 2 - 1 : 0];

    wire [31:0] m0_rslt_1;
    wire [31:0] m1_rslt_1;
    wire [31:0] m2_rslt_1;
    wire [31:0] m3_rslt_1;
    wire [31:0] m4_rslt_1;
    wire [31:0] m5_rslt_1;
    wire [31:0] m6_rslt_1;
    wire [31:0] m7_rslt_1;
    wire [31:0] m8_rslt_1;
    wire [31:0] m9_rslt_1;
    wire [31:0] m10_rslt_1;
    wire [31:0] m11_rslt_1;
    wire [31:0] m12_rslt_1;
    wire [31:0] m13_rslt_1;
    wire [31:0] m14_rslt_1;
    wire [31:0] m15_rslt_1;

    wire [31:0] m0_rslt_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m1_rslt_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m2_rslt_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m3_rslt_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m4_rslt_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m5_rslt_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m6_rslt_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m7_rslt_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m8_rslt_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m9_rslt_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m10_rslt_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m11_rslt_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m12_rslt_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m13_rslt_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m14_rslt_1_reg[STG_NUM - 1 : 0];
    wire [31:0] m15_rslt_1_reg[STG_NUM - 1 : 0];

    wire [31:0] m0_rslt_1_out[STG_NUM - 1 : 0];
    wire [31:0] m1_rslt_1_out[STG_NUM - 1 : 0];
    wire [31:0] m2_rslt_1_out[STG_NUM - 1 : 0];
    wire [31:0] m3_rslt_1_out[STG_NUM - 1 : 0];
    wire [31:0] m4_rslt_1_out[STG_NUM - 1 : 0];
    wire [31:0] m5_rslt_1_out[STG_NUM - 1 : 0];
    wire [31:0] m6_rslt_1_out[STG_NUM - 1 : 0];
    wire [31:0] m7_rslt_1_out[STG_NUM - 1 : 0];
    wire [31:0] m8_rslt_1_out[STG_NUM - 1 : 0];
    wire [31:0] m9_rslt_1_out[STG_NUM - 1 : 0];
    wire [31:0] m10_rslt_1_out[STG_NUM - 1 : 0];
    wire [31:0] m11_rslt_1_out[STG_NUM - 1 : 0];
    wire [31:0] m12_rslt_1_out[STG_NUM - 1 : 0];
    wire [31:0] m13_rslt_1_out[STG_NUM - 1 : 0];
    wire [31:0] m14_rslt_1_out[STG_NUM - 1 : 0];
    wire [31:0] m15_rslt_1_out[STG_NUM - 1 : 0];

    wire [31:0] stg_1_a_buffer[STG_NUM - 1:0];
    wire [31:0] stg_1_b_buffer[STG_NUM - 1:0];
    wire [31:0] stg_1_c_buffer[STG_NUM - 1:0];
    wire [31:0] stg_1_d_buffer[STG_NUM - 1:0];
    wire [31:0] stg_1_e_buffer[STG_NUM - 1:0];
    wire [31:0] stg_1_f_buffer[STG_NUM - 1:0];
    wire [31:0] stg_1_g_buffer[STG_NUM - 1:0];
    wire [31:0] stg_1_h_buffer[STG_NUM - 1:0];

    wire [255:0] rslt_1;
    wire [511:0] rslt_1_chunk;
    assign rslt_1 = {(pipe_reg_a[STG_NUM * 2 - 1] + stg_1_a_buffer[STG_NUM - 1]), (pipe_reg_b[STG_NUM * 2 - 1] + stg_1_b_buffer[STG_NUM - 1]), (pipe_reg_c[STG_NUM * 2 - 1] + stg_1_c_buffer[STG_NUM - 1]), (pipe_reg_d[STG_NUM * 2 - 1] + stg_1_d_buffer[STG_NUM - 1]), (pipe_reg_e[STG_NUM * 2 -1] + stg_1_e_buffer[STG_NUM - 1]), (pipe_reg_f[STG_NUM * 2 - 1] + stg_1_f_buffer[STG_NUM - 1]), (pipe_reg_g[STG_NUM * 2 - 1] + stg_1_g_buffer[STG_NUM - 1]), (pipe_reg_h[STG_NUM * 2 - 1] + stg_1_h_buffer[STG_NUM - 1])};
    assign rslt_1_chunk = {256'h0001000000000000000000000000000000000000000000000000000000000080, rslt_1[7:0], rslt_1[15:8], rslt_1[23:16], rslt_1[31:24], rslt_1[39:32], rslt_1[47:40], rslt_1[55:48], rslt_1[63:56], rslt_1[71:64], rslt_1[79:72], rslt_1[87:80], rslt_1[95:88], rslt_1[103:96], rslt_1[111:104], rslt_1[119:112], rslt_1[127:120], rslt_1[135:128], rslt_1[143:136], rslt_1[151:144], rslt_1[159:152], rslt_1[167:160], rslt_1[175:168], rslt_1[183:176], rslt_1[191:184], rslt_1[199:192], rslt_1[207:200], rslt_1[215:208], rslt_1[223:216], rslt_1[231:224], rslt_1[239:232], rslt_1[247:240], rslt_1[255:248]};

    assign m0_chk_1 = {chunk1[7:0], chunk1[15:8], chunk1[23:16], chunk1[31:24]};
    assign m1_chk_1 = {chunk1[39:32], chunk1[47:40], chunk1[55:48], chunk1[63:56]};
    assign m2_chk_1 = {chunk1[71:64], chunk1[79:72], chunk1[87:80], chunk1[95:88]};
    assign m3_chk_1 = {chunk1[103:96], chunk1[111:104], chunk1[119:112], chunk1[127:120]};
    assign m4_chk_1 = {chunk1[135:128], chunk1[143:136], chunk1[151:144], chunk1[159:152]};
    assign m5_chk_1 = {chunk1[167:160], chunk1[175:168], chunk1[183:176], chunk1[191:184]};
    assign m6_chk_1 = {chunk1[199:192], chunk1[207:200], chunk1[215:208], chunk1[223:216]};
    assign m7_chk_1 = {chunk1[231:224], chunk1[239:232], chunk1[247:240], chunk1[255:248]};
    assign m8_chk_1 = {chunk1[263:256], chunk1[271:264], chunk1[279:272], chunk1[287:280]};
    assign m9_chk_1 = {chunk1[295:288], chunk1[303:296], chunk1[311:304], chunk1[319:312]};
    assign m10_chk_1 = {chunk1[327:320], chunk1[335:328], chunk1[343:336], chunk1[351:344]};
    assign m11_chk_1 = {chunk1[359:352], chunk1[367:360], chunk1[375:368], chunk1[383:376]};
    assign m12_chk_1 = {chunk1[391:384], chunk1[399:392], chunk1[407:400], chunk1[415:408]};
    assign m13_chk_1 = {chunk1[423:416], chunk1[431:424], chunk1[439:432], chunk1[447:440]};
    assign m14_chk_1 = {chunk1[455:448], chunk1[463:456], chunk1[471:464], chunk1[479:472]};
    assign m15_chk_1 = {chunk1[487:480], chunk1[495:488], chunk1[503:496], chunk1[511:504]};

    assign m0_chk_2 = {chunk2[7:0], chunk2[15:8], chunk2[23:16], chunk2[31:24]};
    assign m1_chk_2 = {chunk2[39:32], chunk2[47:40], chunk2[55:48], chunk2[63:56]};
    assign m2_chk_2 = {chunk2[71:64], chunk2[79:72], chunk2[87:80], chunk2[95:88]};
    assign m3_chk_2 = {chunk2[103:96], chunk2[111:104], chunk2[119:112], chunk2[127:120]};
    assign m4_chk_2 = {chunk2[135:128], chunk2[143:136], chunk2[151:144], chunk2[159:152]};
    assign m5_chk_2 = {chunk2[167:160], chunk2[175:168], chunk2[183:176], chunk2[191:184]};
    assign m6_chk_2 = {chunk2[199:192], chunk2[207:200], chunk2[215:208], chunk2[223:216]};
    assign m7_chk_2 = {chunk2[231:224], chunk2[239:232], chunk2[247:240], chunk2[255:248]};
    assign m8_chk_2 = {chunk2[263:256], chunk2[271:264], chunk2[279:272], chunk2[287:280]};
    assign m9_chk_2 = {chunk2[295:288], chunk2[303:296], chunk2[311:304], chunk2[319:312]};
    assign m10_chk_2 = {chunk2[327:320], chunk2[335:328], chunk2[343:336], chunk2[351:344]};
    assign m11_chk_2 = {chunk2[359:352], chunk2[367:360], chunk2[375:368], chunk2[383:376]};
    assign m12_chk_2 = {chunk2[391:384], chunk2[399:392], chunk2[407:400], chunk2[415:408]};
    assign m13_chk_2 = {chunk2[423:416], chunk2[431:424], chunk2[439:432], chunk2[447:440]};
    assign m14_chk_2 = {chunk2[455:448], chunk2[463:456], chunk2[471:464], chunk2[479:472]};
    assign m15_chk_2 = {chunk2[487:480], chunk2[495:488], chunk2[503:496], chunk2[511:504]};

    assign m0_rslt_1 = {rslt_1_chunk[7:0], rslt_1_chunk[15:8], rslt_1_chunk[23:16], rslt_1_chunk[31:24]};
    assign m1_rslt_1 = {rslt_1_chunk[39:32], rslt_1_chunk[47:40], rslt_1_chunk[55:48], rslt_1_chunk[63:56]};
    assign m2_rslt_1 = {rslt_1_chunk[71:64], rslt_1_chunk[79:72], rslt_1_chunk[87:80], rslt_1_chunk[95:88]};
    assign m3_rslt_1 = {rslt_1_chunk[103:96], rslt_1_chunk[111:104], rslt_1_chunk[119:112], rslt_1_chunk[127:120]};
    assign m4_rslt_1 = {rslt_1_chunk[135:128], rslt_1_chunk[143:136], rslt_1_chunk[151:144], rslt_1_chunk[159:152]};
    assign m5_rslt_1 = {rslt_1_chunk[167:160], rslt_1_chunk[175:168], rslt_1_chunk[183:176], rslt_1_chunk[191:184]};
    assign m6_rslt_1 = {rslt_1_chunk[199:192], rslt_1_chunk[207:200], rslt_1_chunk[215:208], rslt_1_chunk[223:216]};
    assign m7_rslt_1 = {rslt_1_chunk[231:224], rslt_1_chunk[239:232], rslt_1_chunk[247:240], rslt_1_chunk[255:248]};
    assign m8_rslt_1 = {rslt_1_chunk[263:256], rslt_1_chunk[271:264], rslt_1_chunk[279:272], rslt_1_chunk[287:280]};
    assign m9_rslt_1 = {rslt_1_chunk[295:288], rslt_1_chunk[303:296], rslt_1_chunk[311:304], rslt_1_chunk[319:312]};
    assign m10_rslt_1 = {rslt_1_chunk[327:320], rslt_1_chunk[335:328], rslt_1_chunk[343:336], rslt_1_chunk[351:344]};
    assign m11_rslt_1 = {rslt_1_chunk[359:352], rslt_1_chunk[367:360], rslt_1_chunk[375:368], rslt_1_chunk[383:376]};
    assign m12_rslt_1 = {rslt_1_chunk[391:384], rslt_1_chunk[399:392], rslt_1_chunk[407:400], rslt_1_chunk[415:408]};
    assign m13_rslt_1 = {rslt_1_chunk[423:416], rslt_1_chunk[431:424], rslt_1_chunk[439:432], rslt_1_chunk[447:440]};
    assign m14_rslt_1 = {rslt_1_chunk[455:448], rslt_1_chunk[463:456], rslt_1_chunk[471:464], rslt_1_chunk[479:472]};
    assign m15_rslt_1 = {rslt_1_chunk[487:480], rslt_1_chunk[495:488], rslt_1_chunk[503:496], rslt_1_chunk[511:504]};

    assign feedback_reset = (feedback_counter == (STG_LOOP - 1));
    assign feedback_pass = (feedback_counter == 0);
    assign buffer_reset = (buffer_counter == 63);
    assign buffer_pass = (buffer_counter == 0);
    
    //SHA256 hash init contant
    localparam H0 = 32'h6a09e667;
    localparam H1 = 32'hbb67ae85;
    localparam H2 = 32'h3c6ef372;
    localparam H3 = 32'ha54ff53a;
    localparam H4 = 32'h510e527f;
    localparam H5 = 32'h9b05688c;
    localparam H6 = 32'h1f83d9ab;
    localparam H7 = 32'h5be0cd19;

    genvar stage;

    //Generate k constant shared by three stages
    generate
        for(stage = 0;stage < STG_NUM;stage = stage + 1)
        begin : kc
            k_pipe kp(.stage(stage), .counter(feedback_counter), .k(k_constant[stage]));
        end
    endgenerate

    //Generate first stage(Hash1Chunk1Stage)
    generate
        for(stage = 0;stage < STG_NUM;stage = stage + 1)
        begin : hash1_chunk1_stage
            sha256_function sf(
                .clk(clk), 
                .rst(rst), 
                .k(k_constant[stage]), 
                .m(m0_chk_1_reg[stage]), 
                .a_in((stage == 0) ? (feedback_pass ? H0 : pipe_reg_a[stage]) : (feedback_pass ? pipe_reg_a[stage - 1] : pipe_reg_a[stage])), 
                .b_in((stage == 0) ? (feedback_pass ? H1 : pipe_reg_b[stage]) : (feedback_pass ? pipe_reg_b[stage - 1] : pipe_reg_b[stage])), 
                .c_in((stage == 0) ? (feedback_pass ? H2 : pipe_reg_c[stage]) : (feedback_pass ? pipe_reg_c[stage - 1] : pipe_reg_c[stage])), 
                .d_in((stage == 0) ? (feedback_pass ? H3 : pipe_reg_d[stage]) : (feedback_pass ? pipe_reg_d[stage - 1] : pipe_reg_d[stage])), 
                .e_in((stage == 0) ? (feedback_pass ? H4 : pipe_reg_e[stage]) : (feedback_pass ? pipe_reg_e[stage - 1] : pipe_reg_e[stage])), 
                .f_in((stage == 0) ? (feedback_pass ? H5 : pipe_reg_f[stage]) : (feedback_pass ? pipe_reg_f[stage - 1] : pipe_reg_f[stage])), 
                .g_in((stage == 0) ? (feedback_pass ? H6 : pipe_reg_g[stage]) : (feedback_pass ? pipe_reg_g[stage - 1] : pipe_reg_g[stage])), 
                .h_in((stage == 0) ? (feedback_pass ? H7 : pipe_reg_h[stage]) : (feedback_pass ? pipe_reg_h[stage - 1] : pipe_reg_h[stage])), 
                .a_out(pipe_reg_a[stage]), 
                .b_out(pipe_reg_b[stage]), 
                .c_out(pipe_reg_c[stage]), 
                .d_out(pipe_reg_d[stage]), 
                .e_out(pipe_reg_e[stage]), 
                .f_out(pipe_reg_f[stage]), 
                .g_out(pipe_reg_g[stage]), 
                .h_out(pipe_reg_h[stage])
            );
            param_function pf(
                .clk(clk),
                .rst(rst), 
                .m0_in((stage == 0) ? (feedback_pass ? m0_chk_1 : m0_chk_1_out[stage]) : (feedback_pass ? m0_chk_1_out[stage - 1] : m0_chk_1_out[stage])), 
                .m1_in((stage == 0) ? (feedback_pass ? m1_chk_1 : m1_chk_1_out[stage]) : (feedback_pass ? m1_chk_1_out[stage - 1] : m1_chk_1_out[stage])), 
                .m2_in((stage == 0) ? (feedback_pass ? m2_chk_1 : m2_chk_1_out[stage]) : (feedback_pass ? m2_chk_1_out[stage - 1] : m2_chk_1_out[stage])), 
                .m3_in((stage == 0) ? (feedback_pass ? m3_chk_1 : m3_chk_1_out[stage]) : (feedback_pass ? m3_chk_1_out[stage - 1] : m3_chk_1_out[stage])), 
                .m4_in((stage == 0) ? (feedback_pass ? m4_chk_1 : m4_chk_1_out[stage]) : (feedback_pass ? m4_chk_1_out[stage - 1] : m4_chk_1_out[stage])), 
                .m5_in((stage == 0) ? (feedback_pass ? m5_chk_1 : m5_chk_1_out[stage]) : (feedback_pass ? m5_chk_1_out[stage - 1] : m5_chk_1_out[stage])), 
                .m6_in((stage == 0) ? (feedback_pass ? m6_chk_1 : m6_chk_1_out[stage]) : (feedback_pass ? m6_chk_1_out[stage - 1] : m6_chk_1_out[stage])), 
                .m7_in((stage == 0) ? (feedback_pass ? m7_chk_1 : m7_chk_1_out[stage]) : (feedback_pass ? m7_chk_1_out[stage - 1] : m7_chk_1_out[stage])), 
                .m8_in((stage == 0) ? (feedback_pass ? m8_chk_1 : m8_chk_1_out[stage]) : (feedback_pass ? m8_chk_1_out[stage - 1] : m8_chk_1_out[stage])), 
                .m9_in((stage == 0) ? (feedback_pass ? m9_chk_1 : m9_chk_1_out[stage]) : (feedback_pass ? m9_chk_1_out[stage - 1] : m9_chk_1_out[stage])), 
                .m10_in((stage == 0) ? (feedback_pass ? m10_chk_1 : m10_chk_1_out[stage]) : (feedback_pass ? m10_chk_1_out[stage - 1] : m10_chk_1_out[stage])), 
                .m11_in((stage == 0) ? (feedback_pass ? m11_chk_1 : m11_chk_1_out[stage]) : (feedback_pass ? m11_chk_1_out[stage - 1] : m11_chk_1_out[stage])), 
                .m12_in((stage == 0) ? (feedback_pass ? m12_chk_1 : m12_chk_1_out[stage]) : (feedback_pass ? m12_chk_1_out[stage - 1] : m12_chk_1_out[stage])), 
                .m13_in((stage == 0) ? (feedback_pass ? m13_chk_1 : m13_chk_1_out[stage]) : (feedback_pass ? m13_chk_1_out[stage - 1] : m13_chk_1_out[stage])), 
                .m14_in((stage == 0) ? (feedback_pass ? m14_chk_1 : m14_chk_1_out[stage]) : (feedback_pass ? m14_chk_1_out[stage - 1] : m14_chk_1_out[stage])), 
                .m15_in((stage == 0) ? (feedback_pass ? m15_chk_1 : m15_chk_1_out[stage]) : (feedback_pass ? m15_chk_1_out[stage - 1] : m15_chk_1_out[stage])), 
                .m0_reg(m0_chk_1_reg[stage]), 
                .m1_reg(m1_chk_1_reg[stage]), 
                .m2_reg(m2_chk_1_reg[stage]), 
                .m3_reg(m3_chk_1_reg[stage]), 
                .m4_reg(m4_chk_1_reg[stage]), 
                .m5_reg(m5_chk_1_reg[stage]), 
                .m6_reg(m6_chk_1_reg[stage]), 
                .m7_reg(m7_chk_1_reg[stage]), 
                .m8_reg(m8_chk_1_reg[stage]), 
                .m9_reg(m9_chk_1_reg[stage]), 
                .m10_reg(m10_chk_1_reg[stage]), 
                .m11_reg(m11_chk_1_reg[stage]), 
                .m12_reg(m12_chk_1_reg[stage]), 
                .m13_reg(m13_chk_1_reg[stage]), 
                .m14_reg(m14_chk_1_reg[stage]), 
                .m15_reg(m15_chk_1_reg[stage]),
                .m0_out(m0_chk_1_out[stage]), 
                .m1_out(m1_chk_1_out[stage]), 
                .m2_out(m2_chk_1_out[stage]), 
                .m3_out(m3_chk_1_out[stage]), 
                .m4_out(m4_chk_1_out[stage]), 
                .m5_out(m5_chk_1_out[stage]), 
                .m6_out(m6_chk_1_out[stage]), 
                .m7_out(m7_chk_1_out[stage]), 
                .m8_out(m8_chk_1_out[stage]), 
                .m9_out(m9_chk_1_out[stage]), 
                .m10_out(m10_chk_1_out[stage]), 
                .m11_out(m11_chk_1_out[stage]), 
                .m12_out(m12_chk_1_out[stage]), 
                .m13_out(m13_chk_1_out[stage]), 
                .m14_out(m14_chk_1_out[stage]), 
                .m15_out(m15_chk_1_out[stage])
            );
            param_buffer pb(
                .clk(clk),
                .rst(rst), 
                .m0_in((stage == 0) ? (feedback_pass ? m0_chk_2 : m0_chk_2_reg[stage]) : (feedback_pass ? m0_chk_2_reg[stage - 1] : m0_chk_2_reg[stage])), 
                .m1_in((stage == 0) ? (feedback_pass ? m1_chk_2 : m1_chk_2_reg[stage]) : (feedback_pass ? m1_chk_2_reg[stage - 1] : m1_chk_2_reg[stage])), 
                .m2_in((stage == 0) ? (feedback_pass ? m2_chk_2 : m2_chk_2_reg[stage]) : (feedback_pass ? m2_chk_2_reg[stage - 1] : m2_chk_2_reg[stage])), 
                .m3_in((stage == 0) ? (feedback_pass ? m3_chk_2 : m3_chk_2_reg[stage]) : (feedback_pass ? m3_chk_2_reg[stage - 1] : m3_chk_2_reg[stage])), 
                .m4_in((stage == 0) ? (feedback_pass ? m4_chk_2 : m4_chk_2_reg[stage]) : (feedback_pass ? m4_chk_2_reg[stage - 1] : m4_chk_2_reg[stage])), 
                .m5_in((stage == 0) ? (feedback_pass ? m5_chk_2 : m5_chk_2_reg[stage]) : (feedback_pass ? m5_chk_2_reg[stage - 1] : m5_chk_2_reg[stage])), 
                .m6_in((stage == 0) ? (feedback_pass ? m6_chk_2 : m6_chk_2_reg[stage]) : (feedback_pass ? m6_chk_2_reg[stage - 1] : m6_chk_2_reg[stage])), 
                .m7_in((stage == 0) ? (feedback_pass ? m7_chk_2 : m7_chk_2_reg[stage]) : (feedback_pass ? m7_chk_2_reg[stage - 1] : m7_chk_2_reg[stage])), 
                .m8_in((stage == 0) ? (feedback_pass ? m8_chk_2 : m8_chk_2_reg[stage]) : (feedback_pass ? m8_chk_2_reg[stage - 1] : m8_chk_2_reg[stage])), 
                .m9_in((stage == 0) ? (feedback_pass ? m9_chk_2 : m9_chk_2_reg[stage]) : (feedback_pass ? m9_chk_2_reg[stage - 1] : m9_chk_2_reg[stage])), 
                .m10_in((stage == 0) ? (feedback_pass ? m10_chk_2 : m10_chk_2_reg[stage]) : (feedback_pass ? m10_chk_2_reg[stage - 1] : m10_chk_2_reg[stage])), 
                .m11_in((stage == 0) ? (feedback_pass ? m11_chk_2 : m11_chk_2_reg[stage]) : (feedback_pass ? m11_chk_2_reg[stage - 1] : m11_chk_2_reg[stage])), 
                .m12_in((stage == 0) ? (feedback_pass ? m12_chk_2 : m12_chk_2_reg[stage]) : (feedback_pass ? m12_chk_2_reg[stage - 1] : m12_chk_2_reg[stage])), 
                .m13_in((stage == 0) ? (feedback_pass ? m13_chk_2 : m13_chk_2_reg[stage]) : (feedback_pass ? m13_chk_2_reg[stage - 1] : m13_chk_2_reg[stage])), 
                .m14_in((stage == 0) ? (feedback_pass ? m14_chk_2 : m14_chk_2_reg[stage]) : (feedback_pass ? m14_chk_2_reg[stage - 1] : m14_chk_2_reg[stage])), 
                .m15_in((stage == 0) ? (feedback_pass ? m15_chk_2 : m15_chk_2_reg[stage]) : (feedback_pass ? m15_chk_2_reg[stage - 1] : m15_chk_2_reg[stage])), 
                .m0_reg(m0_chk_2_reg[stage]), 
                .m1_reg(m1_chk_2_reg[stage]), 
                .m2_reg(m2_chk_2_reg[stage]), 
                .m3_reg(m3_chk_2_reg[stage]), 
                .m4_reg(m4_chk_2_reg[stage]), 
                .m5_reg(m5_chk_2_reg[stage]), 
                .m6_reg(m6_chk_2_reg[stage]), 
                .m7_reg(m7_chk_2_reg[stage]), 
                .m8_reg(m8_chk_2_reg[stage]), 
                .m9_reg(m9_chk_2_reg[stage]), 
                .m10_reg(m10_chk_2_reg[stage]), 
                .m11_reg(m11_chk_2_reg[stage]), 
                .m12_reg(m12_chk_2_reg[stage]), 
                .m13_reg(m13_chk_2_reg[stage]), 
                .m14_reg(m14_chk_2_reg[stage]), 
                .m15_reg(m15_chk_2_reg[stage]),
                .m0_out(m0_chk_2_out[stage]), 
                .m1_out(m1_chk_2_out[stage]), 
                .m2_out(m2_chk_2_out[stage]), 
                .m3_out(m3_chk_2_out[stage]), 
                .m4_out(m4_chk_2_out[stage]), 
                .m5_out(m5_chk_2_out[stage]), 
                .m6_out(m6_chk_2_out[stage]), 
                .m7_out(m7_chk_2_out[stage]), 
                .m8_out(m8_chk_2_out[stage]), 
                .m9_out(m9_chk_2_out[stage]), 
                .m10_out(m10_chk_2_out[stage]), 
                .m11_out(m11_chk_2_out[stage]), 
                .m12_out(m12_chk_2_out[stage]), 
                .m13_out(m13_chk_2_out[stage]), 
                .m14_out(m14_chk_2_out[stage]), 
                .m15_out(m15_chk_2_out[stage])
            );
        end
    endgenerate

    //Generate second stage
    generate
        for(stage = 0;stage < STG_NUM;stage = stage + 1) begin : hash1_chunk2_stage
            sha256_function sf(
                .clk(clk), 
                .rst(rst), 
                .k(k_constant[stage]), 
                .m(m0_chk_2_reg[STG_NUM + stage]), 
                .a_in((stage == 0) ? (feedback_pass ? (pipe_reg_a[STG_NUM + stage - 1] + H0) : pipe_reg_a[STG_NUM + stage]) : (feedback_pass ? pipe_reg_a[STG_NUM + stage - 1] : pipe_reg_a[STG_NUM + stage])), 
                .b_in((stage == 0) ? (feedback_pass ? (pipe_reg_b[STG_NUM + stage - 1] + H1) : pipe_reg_b[STG_NUM + stage]) : (feedback_pass ? pipe_reg_b[STG_NUM + stage - 1] : pipe_reg_b[STG_NUM + stage])), 
                .c_in((stage == 0) ? (feedback_pass ? (pipe_reg_c[STG_NUM + stage - 1] + H2) : pipe_reg_c[STG_NUM + stage]) : (feedback_pass ? pipe_reg_c[STG_NUM + stage - 1] : pipe_reg_c[STG_NUM + stage])), 
                .d_in((stage == 0) ? (feedback_pass ? (pipe_reg_d[STG_NUM + stage - 1] + H3) : pipe_reg_d[STG_NUM + stage]) : (feedback_pass ? pipe_reg_d[STG_NUM + stage - 1] : pipe_reg_d[STG_NUM + stage])), 
                .e_in((stage == 0) ? (feedback_pass ? (pipe_reg_e[STG_NUM + stage - 1] + H4) : pipe_reg_e[STG_NUM + stage]) : (feedback_pass ? pipe_reg_e[STG_NUM + stage - 1] : pipe_reg_e[STG_NUM + stage])), 
                .f_in((stage == 0) ? (feedback_pass ? (pipe_reg_f[STG_NUM + stage - 1] + H5) : pipe_reg_f[STG_NUM + stage]) : (feedback_pass ? pipe_reg_f[STG_NUM + stage - 1] : pipe_reg_f[STG_NUM + stage])), 
                .g_in((stage == 0) ? (feedback_pass ? (pipe_reg_g[STG_NUM + stage - 1] + H6) : pipe_reg_g[STG_NUM + stage]) : (feedback_pass ? pipe_reg_g[STG_NUM + stage - 1] : pipe_reg_g[STG_NUM + stage])), 
                .h_in((stage == 0) ? (feedback_pass ? (pipe_reg_h[STG_NUM + stage - 1] + H7) : pipe_reg_h[STG_NUM + stage]) : (feedback_pass ? pipe_reg_h[STG_NUM + stage - 1] : pipe_reg_h[STG_NUM + stage])), 
                .a_out(pipe_reg_a[STG_NUM + stage]), 
                .b_out(pipe_reg_b[STG_NUM + stage]), 
                .c_out(pipe_reg_c[STG_NUM + stage]), 
                .d_out(pipe_reg_d[STG_NUM + stage]), 
                .e_out(pipe_reg_e[STG_NUM + stage]), 
                .f_out(pipe_reg_f[STG_NUM + stage]), 
                .g_out(pipe_reg_g[STG_NUM + stage]), 
                .h_out(pipe_reg_h[STG_NUM + stage])
            );
            param_function pf(
                .clk(clk),
                .rst(rst), 
                .m0_in(feedback_pass ? m0_chk_2_out[STG_NUM + stage - 1] : m0_chk_2_out[STG_NUM + stage]), 
                .m1_in(feedback_pass ? m1_chk_2_out[STG_NUM + stage - 1] : m1_chk_2_out[STG_NUM + stage]), 
                .m2_in(feedback_pass ? m2_chk_2_out[STG_NUM + stage - 1] : m2_chk_2_out[STG_NUM + stage]), 
                .m3_in(feedback_pass ? m3_chk_2_out[STG_NUM + stage - 1] : m3_chk_2_out[STG_NUM + stage]), 
                .m4_in(feedback_pass ? m4_chk_2_out[STG_NUM + stage - 1] : m4_chk_2_out[STG_NUM + stage]), 
                .m5_in(feedback_pass ? m5_chk_2_out[STG_NUM + stage - 1] : m5_chk_2_out[STG_NUM + stage]), 
                .m6_in(feedback_pass ? m6_chk_2_out[STG_NUM + stage - 1] : m6_chk_2_out[STG_NUM + stage]), 
                .m7_in(feedback_pass ? m7_chk_2_out[STG_NUM + stage - 1] : m7_chk_2_out[STG_NUM + stage]), 
                .m8_in(feedback_pass ? m8_chk_2_out[STG_NUM + stage - 1] : m8_chk_2_out[STG_NUM + stage]), 
                .m9_in(feedback_pass ? m9_chk_2_out[STG_NUM + stage - 1] : m9_chk_2_out[STG_NUM + stage]), 
                .m10_in(feedback_pass ? m10_chk_2_out[STG_NUM + stage - 1] : m10_chk_2_out[STG_NUM + stage]), 
                .m11_in(feedback_pass ? m11_chk_2_out[STG_NUM + stage - 1] : m11_chk_2_out[STG_NUM + stage]), 
                .m12_in(feedback_pass ? m12_chk_2_out[STG_NUM + stage - 1] : m12_chk_2_out[STG_NUM + stage]), 
                .m13_in(feedback_pass ? m13_chk_2_out[STG_NUM + stage - 1] : m13_chk_2_out[STG_NUM + stage]), 
                .m14_in(feedback_pass ? m14_chk_2_out[STG_NUM + stage - 1] : m14_chk_2_out[STG_NUM + stage]), 
                .m15_in(feedback_pass ? m15_chk_2_out[STG_NUM + stage - 1] : m15_chk_2_out[STG_NUM + stage]), 
                .m0_reg(m0_chk_2_reg[STG_NUM + stage]), 
                .m1_reg(m1_chk_2_reg[STG_NUM + stage]), 
                .m2_reg(m2_chk_2_reg[STG_NUM + stage]), 
                .m3_reg(m3_chk_2_reg[STG_NUM + stage]), 
                .m4_reg(m4_chk_2_reg[STG_NUM + stage]), 
                .m5_reg(m5_chk_2_reg[STG_NUM + stage]), 
                .m6_reg(m6_chk_2_reg[STG_NUM + stage]), 
                .m7_reg(m7_chk_2_reg[STG_NUM + stage]), 
                .m8_reg(m8_chk_2_reg[STG_NUM + stage]), 
                .m9_reg(m9_chk_2_reg[STG_NUM + stage]), 
                .m10_reg(m10_chk_2_reg[STG_NUM + stage]), 
                .m11_reg(m11_chk_2_reg[STG_NUM + stage]), 
                .m12_reg(m12_chk_2_reg[STG_NUM + stage]), 
                .m13_reg(m13_chk_2_reg[STG_NUM + stage]), 
                .m14_reg(m14_chk_2_reg[STG_NUM + stage]), 
                .m15_reg(m15_chk_2_reg[STG_NUM + stage]),
                .m0_out(m0_chk_2_out[STG_NUM + stage]), 
                .m1_out(m1_chk_2_out[STG_NUM + stage]), 
                .m2_out(m2_chk_2_out[STG_NUM + stage]), 
                .m3_out(m3_chk_2_out[STG_NUM + stage]), 
                .m4_out(m4_chk_2_out[STG_NUM + stage]), 
                .m5_out(m5_chk_2_out[STG_NUM + stage]), 
                .m6_out(m6_chk_2_out[STG_NUM + stage]), 
                .m7_out(m7_chk_2_out[STG_NUM + stage]), 
                .m8_out(m8_chk_2_out[STG_NUM + stage]), 
                .m9_out(m9_chk_2_out[STG_NUM + stage]), 
                .m10_out(m10_chk_2_out[STG_NUM + stage]), 
                .m11_out(m11_chk_2_out[STG_NUM + stage]), 
                .m12_out(m12_chk_2_out[STG_NUM + stage]), 
                .m13_out(m13_chk_2_out[STG_NUM + stage]), 
                .m14_out(m14_chk_2_out[STG_NUM + stage]), 
                .m15_out(m15_chk_2_out[STG_NUM + stage])
            );
            stg1_buffer sb0(
                .clk(clk),
                .rst(rst),
                .a_in((stage == 0) ? (feedback_pass ? (pipe_reg_a[STG_NUM + stage - 1] + H0) : stg_1_a_buffer[stage]) : (feedback_pass ? stg_1_a_buffer[stage - 1] : stg_1_a_buffer[stage])), 
                .b_in((stage == 0) ? (feedback_pass ? (pipe_reg_b[STG_NUM + stage - 1] + H1) : stg_1_b_buffer[stage]) : (feedback_pass ? stg_1_b_buffer[stage - 1] : stg_1_b_buffer[stage])), 
                .c_in((stage == 0) ? (feedback_pass ? (pipe_reg_c[STG_NUM + stage - 1] + H2) : stg_1_c_buffer[stage]) : (feedback_pass ? stg_1_c_buffer[stage - 1] : stg_1_c_buffer[stage])), 
                .d_in((stage == 0) ? (feedback_pass ? (pipe_reg_d[STG_NUM + stage - 1] + H3) : stg_1_d_buffer[stage]) : (feedback_pass ? stg_1_d_buffer[stage - 1] : stg_1_d_buffer[stage])), 
                .e_in((stage == 0) ? (feedback_pass ? (pipe_reg_e[STG_NUM + stage - 1] + H4) : stg_1_e_buffer[stage]) : (feedback_pass ? stg_1_e_buffer[stage - 1] : stg_1_e_buffer[stage])), 
                .f_in((stage == 0) ? (feedback_pass ? (pipe_reg_f[STG_NUM + stage - 1] + H5) : stg_1_f_buffer[stage]) : (feedback_pass ? stg_1_f_buffer[stage - 1] : stg_1_f_buffer[stage])), 
                .g_in((stage == 0) ? (feedback_pass ? (pipe_reg_g[STG_NUM + stage - 1] + H6) : stg_1_g_buffer[stage]) : (feedback_pass ? stg_1_g_buffer[stage - 1] : stg_1_g_buffer[stage])), 
                .h_in((stage == 0) ? (feedback_pass ? (pipe_reg_h[STG_NUM + stage - 1] + H7) : stg_1_h_buffer[stage]) : (feedback_pass ? stg_1_h_buffer[stage - 1] : stg_1_h_buffer[stage])), 
                .a_reg(stg_1_a_buffer[stage]), 
                .b_reg(stg_1_b_buffer[stage]), 
                .c_reg(stg_1_c_buffer[stage]), 
                .d_reg(stg_1_d_buffer[stage]), 
                .e_reg(stg_1_e_buffer[stage]), 
                .f_reg(stg_1_f_buffer[stage]), 
                .g_reg(stg_1_g_buffer[stage]), 
                .h_reg(stg_1_h_buffer[stage])
            );
        end
    endgenerate
    
    //Generate third stage
    generate
        for(stage = 0;stage < STG_NUM;stage = stage + 1) begin : hash2_chunk1_stage
            sha256_function sf(
                .clk(clk), 
                .rst(rst), 
                .k(k_constant[stage]), 
                .m(m0_rslt_1_reg[stage]), 
                .a_in((stage == 0) ? (feedback_pass ? H0 : pipe_reg_a[2 * STG_NUM + stage]) : (feedback_pass ? pipe_reg_a[2 * STG_NUM + stage - 1] : pipe_reg_a[2 * STG_NUM + stage])), 
                .b_in((stage == 0) ? (feedback_pass ? H1 : pipe_reg_b[2 * STG_NUM + stage]) : (feedback_pass ? pipe_reg_b[2 * STG_NUM + stage - 1] : pipe_reg_b[2 * STG_NUM + stage])), 
                .c_in((stage == 0) ? (feedback_pass ? H2 : pipe_reg_c[2 * STG_NUM + stage]) : (feedback_pass ? pipe_reg_c[2 * STG_NUM + stage - 1] : pipe_reg_c[2 * STG_NUM + stage])), 
                .d_in((stage == 0) ? (feedback_pass ? H3 : pipe_reg_d[2 * STG_NUM + stage]) : (feedback_pass ? pipe_reg_d[2 * STG_NUM + stage - 1] : pipe_reg_d[2 * STG_NUM + stage])), 
                .e_in((stage == 0) ? (feedback_pass ? H4 : pipe_reg_e[2 * STG_NUM + stage]) : (feedback_pass ? pipe_reg_e[2 * STG_NUM + stage - 1] : pipe_reg_e[2 * STG_NUM + stage])), 
                .f_in((stage == 0) ? (feedback_pass ? H5 : pipe_reg_f[2 * STG_NUM + stage]) : (feedback_pass ? pipe_reg_f[2 * STG_NUM + stage - 1] : pipe_reg_f[2 * STG_NUM + stage])), 
                .g_in((stage == 0) ? (feedback_pass ? H6 : pipe_reg_g[2 * STG_NUM + stage]) : (feedback_pass ? pipe_reg_g[2 * STG_NUM + stage - 1] : pipe_reg_g[2 * STG_NUM + stage])), 
                .h_in((stage == 0) ? (feedback_pass ? H7 : pipe_reg_h[2 * STG_NUM + stage]) : (feedback_pass ? pipe_reg_h[2 * STG_NUM + stage - 1] : pipe_reg_h[2 * STG_NUM + stage])), 
                .a_out(pipe_reg_a[2 * STG_NUM + stage]), 
                .b_out(pipe_reg_b[2 * STG_NUM + stage]), 
                .c_out(pipe_reg_c[2 * STG_NUM + stage]), 
                .d_out(pipe_reg_d[2 * STG_NUM + stage]), 
                .e_out(pipe_reg_e[2 * STG_NUM + stage]), 
                .f_out(pipe_reg_f[2 * STG_NUM + stage]), 
                .g_out(pipe_reg_g[2 * STG_NUM + stage]), 
                .h_out(pipe_reg_h[2 * STG_NUM + stage])
            );
            param_function pf(
                .clk(clk),
                .rst(rst), 
                .m0_in((stage == 0) ? (feedback_pass ? m0_rslt_1 : m0_rslt_1_out[stage]) : (feedback_pass ? m0_rslt_1_out[stage - 1] : m0_rslt_1_out[stage])), 
                .m1_in((stage == 0) ? (feedback_pass ? m1_rslt_1 : m1_rslt_1_out[stage]) : (feedback_pass ? m1_rslt_1_out[stage - 1] : m1_rslt_1_out[stage])), 
                .m2_in((stage == 0) ? (feedback_pass ? m2_rslt_1 : m2_rslt_1_out[stage]) : (feedback_pass ? m2_rslt_1_out[stage - 1] : m2_rslt_1_out[stage])), 
                .m3_in((stage == 0) ? (feedback_pass ? m3_rslt_1 : m3_rslt_1_out[stage]) : (feedback_pass ? m3_rslt_1_out[stage - 1] : m3_rslt_1_out[stage])), 
                .m4_in((stage == 0) ? (feedback_pass ? m4_rslt_1 : m4_rslt_1_out[stage]) : (feedback_pass ? m4_rslt_1_out[stage - 1] : m4_rslt_1_out[stage])), 
                .m5_in((stage == 0) ? (feedback_pass ? m5_rslt_1 : m5_rslt_1_out[stage]) : (feedback_pass ? m5_rslt_1_out[stage - 1] : m5_rslt_1_out[stage])), 
                .m6_in((stage == 0) ? (feedback_pass ? m6_rslt_1 : m6_rslt_1_out[stage]) : (feedback_pass ? m6_rslt_1_out[stage - 1] : m6_rslt_1_out[stage])), 
                .m7_in((stage == 0) ? (feedback_pass ? m7_rslt_1 : m7_rslt_1_out[stage]) : (feedback_pass ? m7_rslt_1_out[stage - 1] : m7_rslt_1_out[stage])), 
                .m8_in((stage == 0) ? (feedback_pass ? m8_rslt_1 : m8_rslt_1_out[stage]) : (feedback_pass ? m8_rslt_1_out[stage - 1] : m8_rslt_1_out[stage])), 
                .m9_in((stage == 0) ? (feedback_pass ? m9_rslt_1 : m9_rslt_1_out[stage]) : (feedback_pass ? m9_rslt_1_out[stage - 1] : m9_rslt_1_out[stage])), 
                .m10_in((stage == 0) ? (feedback_pass ? m10_rslt_1 : m10_rslt_1_out[stage]) : (feedback_pass ? m10_rslt_1_out[stage - 1] : m10_rslt_1_out[stage])), 
                .m11_in((stage == 0) ? (feedback_pass ? m11_rslt_1 : m11_rslt_1_out[stage]) : (feedback_pass ? m11_rslt_1_out[stage - 1] : m11_rslt_1_out[stage])), 
                .m12_in((stage == 0) ? (feedback_pass ? m12_rslt_1 : m12_rslt_1_out[stage]) : (feedback_pass ? m12_rslt_1_out[stage - 1] : m12_rslt_1_out[stage])), 
                .m13_in((stage == 0) ? (feedback_pass ? m13_rslt_1 : m13_rslt_1_out[stage]) : (feedback_pass ? m13_rslt_1_out[stage - 1] : m13_rslt_1_out[stage])), 
                .m14_in((stage == 0) ? (feedback_pass ? m14_rslt_1 : m14_rslt_1_out[stage]) : (feedback_pass ? m14_rslt_1_out[stage - 1] : m14_rslt_1_out[stage])), 
                .m15_in((stage == 0) ? (feedback_pass ? m15_rslt_1 : m15_rslt_1_out[stage]) : (feedback_pass ? m15_rslt_1_out[stage - 1] : m15_rslt_1_out[stage])), 
                .m0_out(m0_rslt_1_out[stage]), 
                .m1_out(m1_rslt_1_out[stage]), 
                .m2_out(m2_rslt_1_out[stage]), 
                .m3_out(m3_rslt_1_out[stage]), 
                .m4_out(m4_rslt_1_out[stage]), 
                .m5_out(m5_rslt_1_out[stage]), 
                .m6_out(m6_rslt_1_out[stage]), 
                .m7_out(m7_rslt_1_out[stage]), 
                .m8_out(m8_rslt_1_out[stage]), 
                .m9_out(m9_rslt_1_out[stage]), 
                .m10_out(m10_rslt_1_out[stage]), 
                .m11_out(m11_rslt_1_out[stage]), 
                .m12_out(m12_rslt_1_out[stage]), 
                .m13_out(m13_rslt_1_out[stage]), 
                .m14_out(m14_rslt_1_out[stage]), 
                .m15_out(m15_rslt_1_out[stage]),

                .m0_reg(m0_rslt_1_reg[stage]), 
                .m1_reg(m1_rslt_1_reg[stage]), 
                .m2_reg(m2_rslt_1_reg[stage]), 
                .m3_reg(m3_rslt_1_reg[stage]), 
                .m4_reg(m4_rslt_1_reg[stage]), 
                .m5_reg(m5_rslt_1_reg[stage]), 
                .m6_reg(m6_rslt_1_reg[stage]), 
                .m7_reg(m7_rslt_1_reg[stage]), 
                .m8_reg(m8_rslt_1_reg[stage]), 
                .m9_reg(m9_rslt_1_reg[stage]), 
                .m10_reg(m10_rslt_1_reg[stage]), 
                .m11_reg(m11_rslt_1_reg[stage]), 
                .m12_reg(m12_rslt_1_reg[stage]), 
                .m13_reg(m13_rslt_1_reg[stage]), 
                .m14_reg(m14_rslt_1_reg[stage]), 
                .m15_reg(m15_rslt_1_reg[stage])
            );
        end
    endgenerate

    //Feedback counter
    always @ (posedge clk) begin
        if(rst | pipe_init)
            feedback_counter = 0;
        else if(feedback_reset)
            feedback_counter = 0;
        else
            feedback_counter = feedback_counter + 1;
    end

    //Buffer counter
    always @ (posedge clk) begin
        if(rst | pipe_init)
            buffer_counter = 6'b0;
        else if(buffer_reset)
            buffer_counter = 6'b0;
        else
            buffer_counter = buffer_counter + 6'b1;
    end

    //Init counter
    always @ (posedge clk) begin
        if(rst | pipe_init) begin
            init_counter = 8'b0;
        end
        else begin
            init_counter = init_counter + 8'b1;
        end
    end

    //Ready to output
    always @ (posedge clk) begin
        if(rst | pipe_init) begin
            pipeout_ready = 1'b0;
        end
        else begin
            if(!pipeout_ready) begin
                if(init_counter >= 192) begin
                    pipeout_ready = 1'b1;
                end
                else begin
                    pipeout_ready = 1'b0;
                end
            end
            else begin
                pipeout_ready = 1'b1;
            end
        end
    end

    //Output Registers
    assign a_out = pipe_reg_a[3 * STG_NUM - 1] + H0;
    assign b_out = pipe_reg_b[3 * STG_NUM - 1] + H1;
    assign c_out = pipe_reg_c[3 * STG_NUM - 1] + H2;
    assign d_out = pipe_reg_d[3 * STG_NUM - 1] + H3;
    assign e_out = pipe_reg_e[3 * STG_NUM - 1] + H4;
    assign f_out = pipe_reg_f[3 * STG_NUM - 1] + H5;
    assign g_out = pipe_reg_g[3 * STG_NUM - 1] + H6;
    assign h_out = pipe_reg_h[3 * STG_NUM - 1] + H7;

    //Output Signals
    assign output_valid = pipeout_ready && (feedback_counter == 0);
    assign output_data = {a_out, b_out, c_out, d_out, e_out, f_out, g_out, h_out};
endmodule
