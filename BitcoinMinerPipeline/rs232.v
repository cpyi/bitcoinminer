/*  RS232 RX/TX module
    Modified from example in the book 兼容ARM9的軟核處理器設計基於FPGA(李新兵)
    Assume baud rate is 9600 Hz and clock is 25 MHz, modify parameter PERIOD and HPERIOD if needed.
        PERIOD = (clock)/(baud rate), HPERIOD = (clock)/(baud rate)/2
    No parity check of received data.
    The signal rx_vld rises only for one clock when recieved data is prepared.
*/
module rs232(clk ,rst, rx, tx_vld, tx_data, rx_vld, rx_data, tx, tx_rdy);
    input clk;
    input rst;
    input rx;
    input tx_vld;
    input [7:0] tx_data;
    output reg rx_vld;
    output reg [7:0] rx_data;
    output reg tx;
    output tx_rdy; 
    parameter PERIOD = 14'd100;
    parameter HPERIOD = 14'd50;
    reg rx1, rx2, rx3, rxx, rx_dly;
    wire rx_change;
    reg [13:0] rx_cnt;
    wire rx_en;
    reg data_vld;
    reg [3:0] data_cnt;
    reg [7:0] tx_rdy_data;
    reg tran_vld;
    reg [3:0] tran_cnt;
    //Avoid metastability
    always @ (posedge clk) begin
        rx1 <= rx;
        rx2 <= rx1;
        rx3 <= rx2;
        rxx <= rx3;
    end

    //Change
    always @ (posedge clk) begin
        rx_dly <= rxx;
    end
    assign rx_change = (rxx != rx_dly);

    //rx counter
    always @ (posedge clk) begin
        if(rst)
            rx_cnt <= 0;
        else if(rx_change | (rx_cnt == PERIOD))
            rx_cnt <= 0;
        else
            rx_cnt <= rx_cnt + 1'b1;
    end
    
    //Sample time
    assign rx_en = (rx_cnt == HPERIOD);

    //RX change to 0 imply start to transfer data
    always @ (posedge clk) begin
        if(rst)
            data_vld <= 1'b0;
        else if(rx_en & ~rxx & ~data_vld)
            data_vld <= 1'b1;
        else if(data_vld & (data_cnt == 4'h9) & rx_en)
            data_vld <= 1'b0;
        else 
            data_vld <= data_vld;
    end

    //RX data count
    always @ (posedge clk) begin
        if(rst)
            data_cnt <= 0;
        else if(data_vld)
            if(rx_en)
                data_cnt <= data_cnt + 1'b1;
            else
                data_cnt <= data_cnt;
        else
            data_cnt <= 0;
    end
    
    //RX data
    always @ (posedge clk) begin
        if(rst)
            rx_data <= 7'b0;
        else if(data_vld & rx_en & ~data_cnt[3])
            rx_data <= {rxx, rx_data[7:1]};
        else
            rx_data <= rx_data;
    end

    //RX data valid
    always @ (posedge clk) begin
        if(rst)
            rx_vld <= 1'b0;
        else
            rx_vld <= data_vld & rx_en & (data_cnt == 4'h9);
    end

    //Save the data when ready and input presented
    always @ (posedge clk) begin
        if(rst)
            tx_rdy_data <= 8'b0;
        else if(tx_vld & tx_rdy)
            tx_rdy_data <= tx_data;
        else
            tx_rdy_data <= tx_rdy_data;
    end
    
    //Transmission process
    always @ (posedge clk) begin
        if(rst)
            tran_vld <= 1'b0;
        else if(tx_vld)
            tran_vld <= 1'b1;
        else if(tran_vld & rx_en & (tran_cnt == 4'd10))
            tran_vld <= 1'b0;
        else
            tran_vld <= tran_vld;
    end

    //Transmission count
    always @ (posedge clk) begin
        if(rst)
            tran_cnt <= 4'b0;
        else if(tran_vld)
            if(rx_en)
                tran_cnt <= tran_cnt + 1'b1;
            else
                tran_cnt <= tran_cnt;
        else
            tran_cnt <= 4'b0;
    end

    //TX output
    always @ (posedge clk) begin
        if(rst)
            tx <= 1'b1;
        else if(tran_vld)
            if(rx_en)
                case(tran_cnt)
                    4'd0: tx <= 1'b0;
                    4'd1: tx <= tx_rdy_data[0];
                    4'd2: tx <= tx_rdy_data[1];
                    4'd3: tx <= tx_rdy_data[2];
                    4'd4: tx <= tx_rdy_data[3];
                    4'd5: tx <= tx_rdy_data[4];
                    4'd6: tx <= tx_rdy_data[5];
                    4'd7: tx <= tx_rdy_data[6];
                    4'd8: tx <= tx_rdy_data[7];
                    4'd9: tx <= ^tx_rdy_data;
                    4'd10: tx <= 1'b1;
                    default: tx = tx <= 1'b1;
                endcase
            else
                tx <= tx;
        else
            tx <= 1'b1;
    end
    assign tx_rdy = ~tran_vld;
endmodule
