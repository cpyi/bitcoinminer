`timescale 1ns / 1ps
module bitcoin_miner_pipe(clk, rst_n, rx_n, tx_n, dbg_nonce, dbg_rslt, dbg_satisfy);

    //localparam STG_NUM = (64 >> STG_LOOP_LOG);
    localparam STG_NUM = 16;

    //localparam STG_LOOP = (2 << STG_LOOP_LOG);
    localparam STG_LOOP = 4;

    //Pipeline stage parameter
    localparam STG_LOOP_LOG = 2;

    //Port definition
    input clk;
    input rst_n;
    input rx_n;
    output tx_n;
    output [31:0] dbg_nonce;
    output [255:0] dbg_rslt;
    output dbg_satisfy;

    //Negation to fit voltage level
    wire rst;
    wire rx;
    wire tx;
    assign rst = ~rst_n;
    assign rx = ~rx_n;
    assign tx_n = ~tx;

    //Block header without nonce is 76 bytes, nonce is 4 bytes
    reg [607:0] block_header;
    reg [607:0] block_header_next;
    reg [31:0] nonce;
    reg [31:0] nonce_next;

    //Chunks for hash
    wire [511:0] chunk1;
    wire [511:0] chunk2;
    wire pipe_init;
    reg [7:0] pipe_count;
    reg [7:0] pipe_count_next;

    //SHA engine related port
    wire sha_valid;
    wire [255:0] sha_output;
    wire [255:0] hash_result;

    //FSM states
    parameter S_RX = 2'd0;
    parameter S_INIT = 2'd1;
    parameter S_HASH = 2'd2;
    parameter S_TX = 2'd3;
    reg [1:0] curr_state;
    reg [1:0] next_state;

    //RX received bytes counter
    reg [6:0] rx_count;
    reg [6:0] rx_count_next;

    //TX sended bytes counter
    reg [5:0] tx_count;
    reg [5:0] tx_count_next;

    //TX data
    reg tx_vld;
    reg [7:0] tx_data;
    reg [7:0] tx_byte;

    //RX data
    wire [7:0] rx_data;

    //Difficulty
    wire [7:0] difficulty_exp;
    wire [23:0] difficulty_val;
    wire [255:0] difficulty_expanded;
    wire difficulty_satisfy;


    //RS232 module
    rs232 r0(.clk(clk), .rst(rst), .rx(rx), .tx_vld(tx_vld), .tx_data(tx_data), .rx_vld(rx_vld), .rx_data(rx_data), .tx(tx), .tx_rdy(tx_rdy));

    //SHA engine module
    sha256_pipe sp0(.clk(clk), .rst(rst), .pipe_init(pipe_init), .chunk1(chunk1), .chunk2(chunk2), .output_valid(sha_valid), .output_data(sha_output));
    
    //States
    always @ (*) begin
        if(curr_state == S_RX) begin
            if(rx_count == 7'd75 && rx_vld)
                next_state = S_INIT;
            else
                next_state = S_RX;
        end
        else if(curr_state == S_INIT) begin
            next_state = S_HASH;
        end
        else if(curr_state == S_HASH) begin
            if(sha_valid && difficulty_satisfy)
                next_state = S_TX;
            else
                next_state = S_HASH;
        end
        else if(curr_state == S_TX) begin
			if(tx_count == 3 && tx_rdy)
                next_state = S_RX;
            else
                next_state = S_TX;
        end
        else begin
            next_state = S_RX;
        end
    end

    //TX counter
    always @ (*) begin
        if(curr_state == S_INIT)
            tx_count_next = 6'b0;
        else if(curr_state == S_TX && tx_rdy)
            tx_count_next = tx_count + 1;
        else
            tx_count_next = tx_count;
    end

    //RX counter
    always @ (*) begin
        if(curr_state == S_TX) begin
            rx_count_next = 0;
        end
        else if(curr_state == S_RX) begin
            if(rx_vld)
                rx_count_next = rx_count + 1;
            else
                rx_count_next = rx_count;
        end
        else begin
            rx_count_next = rx_count;
        end
    end

    //RX read in block header
    always @ (*) begin
        if(rx_vld)
            block_header_next = {rx_data, block_header[607:8]};
        else
            block_header_next = block_header;
    end

    //Chunk 1 and chunk 2 of stage 1
    assign chunk1 = block_header[511:0];
    assign chunk2 = {384'h800200000000000000000000000000000000000000000000000000000000000000000000000000000000000000000080, nonce, block_header[607:512]};

    //Hash result
    assign hash_result = {sha_output[7:0], sha_output[15:8], sha_output[23:16], sha_output[31:24], sha_output[39:32], sha_output[47:40], sha_output[55:48], sha_output[63:56], sha_output[71:64], sha_output[79:72], sha_output[87:80], sha_output[95:88], sha_output[103:96], sha_output[111:104], sha_output[119:112], sha_output[127:120], sha_output[135:128], sha_output[143:136], sha_output[151:144], sha_output[159:152], sha_output[167:160], sha_output[175:168], sha_output[183:176], sha_output[191:184], sha_output[199:192], sha_output[207:200], sha_output[215:208], sha_output[223:216], sha_output[231:224], sha_output[239:232], sha_output[247:240], sha_output[255:248]};
    

    //Difficulty
    assign difficulty_val = block_header[599:576];
    assign difficulty_exp = block_header[607:600];
    assign difficulty_expanded = 256'b0 | difficulty_val << ((difficulty_exp << 3) - 11'd24);
    assign difficulty_satisfy = (hash_result < difficulty_expanded);

    //Nonce
    always @ (*) begin
        if(curr_state == S_INIT)
            nonce <= 32'd2504433960;//nonce_next = 32'b0;
        else if(curr_state == S_HASH && !difficulty_satisfy && pipe_count == STG_LOOP - 1)
            nonce_next = nonce + 1;
        else
            nonce_next = nonce;
    end

    //Pipeline counter
    always @ (*) begin
        if(curr_state == S_INIT)
            pipe_count_next = 8'b0;
        else if(pipe_count == STG_LOOP - 1)
            pipe_count_next = 8'b0;
        else
            pipe_count_next = pipe_count + 1;
    end
    assign pipe_init = (curr_state == S_INIT);
    
    //Flip flop
    always @ (posedge clk) begin
        if(rst) begin
            block_header <= 608'b0;
            //nonce <= 32'b0;
            nonce <= 32'd2504433960;
            curr_state <= S_RX;
            pipe_count <= 8'b0;
            rx_count <= 7'b0;
            tx_count <= 6'b0;
        end
        else begin
            block_header <= block_header_next;
            nonce <= nonce_next;
            curr_state <= next_state;
            pipe_count <= pipe_count_next;
            rx_count <= rx_count_next;
            tx_count <= tx_count_next;
        end
    end

    //Data for TX module
    always @ (*) begin
        if(curr_state == S_TX && tx_rdy) begin
            tx_vld = 1'b1;
            tx_data = tx_byte;
        end
        else begin
            tx_vld = 1'b0;
            tx_data = 8'b0;
        end
    end

    //Byte to send
    always @ (*) begin
        case(tx_count)
			0: tx_byte = nonce[7:0];
            1: tx_byte = nonce[15:8];
            2: tx_byte = nonce[23:16];
            3: tx_byte = nonce[31:24];
            default: tx_byte = 8'b0;
        endcase
    end
    assign dbg_nonce = nonce;
    assign dbg_rslt = hash_result;
    assign dbg_satisfy = difficulty_satisfy;
    //DBG part(use led on the board, 1->dark, 0->light)
    //assign DBG[6:0] = rx_count[6:0];
    //assign DBG[7] = rx;
endmodule
