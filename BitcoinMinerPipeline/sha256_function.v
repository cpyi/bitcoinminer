`timescale 1ns / 1ps
module sha256_function(clk, rst, k, m, a_in, b_in, c_in, d_in, e_in, f_in, g_in,h_in, a_out, b_out, c_out, d_out, e_out, f_out, g_out, h_out);
    input clk, rst;
    input [31:0] k, m, a_in, b_in, c_in, d_in, e_in, f_in, g_in, h_in;
    reg [31:0] a_reg, b_reg, c_reg, d_reg, e_reg, f_reg, g_reg, h_reg;
    output reg [31:0] a_out, b_out, c_out, d_out, e_out, f_out, g_out, h_out;
    reg [31:0] ch, maj, s0, s1;
    reg [31:0] sum;
    always @ (posedge clk) begin
        if(rst) begin
            a_reg <= 32'b0;
            b_reg <= 32'b0;
            c_reg <= 32'b0;
            d_reg <= 32'b0;
            e_reg <= 32'b0;
            f_reg <= 32'b0;
            g_reg <= 32'b0;
            h_reg <= 32'b0;
        end
        else begin
            a_reg <= a_in;
            b_reg <= b_in;
            c_reg <= c_in;
            d_reg <= d_in;
            e_reg <= e_in;
            f_reg <= f_in;
            g_reg <= g_in;
            h_reg <= h_in;
        end
    end
    always @ (*) begin
        ch = ((e_reg & f_reg) ^ ((~e_reg) & g_reg));
        maj = ((a_reg & b_reg) ^ (a_reg & c_reg) ^ (b_reg & c_reg));
        s0 = ({a_reg[1:0],a_reg[31:2]}) ^ ({a_reg[12:0],a_reg[31:13]}) ^ ({a_reg[21:0],a_reg[31:22]});
        s1 = ({e_reg[5:0],e_reg[31:6]}) ^ ({e_reg[10:0],e_reg[31:11]}) ^ ({e_reg[24:0],e_reg[31:25]});
        sum = h_reg + s1 + ch + k + m;
    end
    always @ (*) begin
        a_out = s0 + maj + sum;
        b_out = a_reg;
        c_out = b_reg;
        d_out = c_reg;
        e_out = d_reg + sum;
        f_out = e_reg;
        g_out = f_reg;
        h_out = g_reg;
    end
endmodule
