`timescale 1ns / 1ps
module stg1_buffer(clk, rst, a_in, b_in, c_in, d_in, e_in, f_in, g_in, h_in, a_reg, b_reg, c_reg, d_reg, e_reg, f_reg, g_reg, h_reg);
    input clk;
    input rst;
    input [31:0] a_in;
    input [31:0] b_in;
    input [31:0] c_in;
    input [31:0] d_in;
    input [31:0] e_in;
    input [31:0] f_in;
    input [31:0] g_in;
    input [31:0] h_in;
    output reg [31:0] a_reg;
    output reg [31:0] b_reg;
    output reg [31:0] c_reg;
    output reg [31:0] d_reg;
    output reg [31:0] e_reg;
    output reg [31:0] f_reg;
    output reg [31:0] g_reg;
    output reg [31:0] h_reg;
    output reg [31:0] a_out;
    output reg [31:0] b_out;
    output reg [31:0] c_out;
    output reg [31:0] d_out;
    output reg [31:0] e_out;
    output reg [31:0] f_out;
    output reg [31:0] g_out;
    output reg [31:0] h_out;
    always @ (posedge clk) begin
        if(rst) begin
            a_reg = 32'b0;
            b_reg = 32'b0;
            c_reg = 32'b0;
            d_reg = 32'b0;
            e_reg = 32'b0;
            f_reg = 32'b0;
            g_reg = 32'b0;
            h_reg = 32'b0;
        end
        else begin
            a_reg = a_in;
            b_reg = b_in;
            c_reg = c_in;
            d_reg = d_in;
            e_reg = e_in;
            f_reg = f_in;
            g_reg = g_in;
            h_reg = h_in;
        end
    end
endmodule
