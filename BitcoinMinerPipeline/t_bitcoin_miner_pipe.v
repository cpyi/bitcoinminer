`timescale 10ns/10ps
module t_bitcoin_miner_pipe();

    //Port definition
    reg clk, rst, rx, rs_clk;
    reg [7:0] byte;
    wire tx;
    wire [31:0] dbg_nonce;
    wire [31:0] nonce;
    wire [255:0] dbg_rslt;
    wire dbg_satisfy;

    //Misc vars
    integer i, j, fpr, r;

    //Module
    bitcoin_miner_pipe bmp0(.clk(clk), .rst_n(~rst), .rx_n(~rx), .tx_n(tx), .dbg_nonce(dbg_nonce), .dbg_rslt(dbg_rslt), .dbg_satisfy(dbg_satisfy));

    //Clock generation
    always #10 clk = ~clk;
    always #1000 rs_clk = ~rs_clk;
    assign nonce = dbg_nonce - 48;

    //Dump file
    initial begin
        //$dumpfile("bitcoin_miner_pipe.vcd");
        //$dumpvars;
    end

    //Monitor
    initial begin
        $monitor(nonce);
    end

    initial begin
        //Open file
        fpr = $fopen("bitcoin_miner_pipe.in", "r");

        //Initialization of registers
        clk = 0;
        rs_clk = 0;
        rst = 0;
        rx = 1;

        //Reset
        @(negedge clk);
        rst = 1;
        @(negedge clk);
        rst = 0;
        @(negedge clk);
        @(posedge rs_clk);
        //Loop
        for(i = 0;i < 76;i = i + 1) begin
            r = $fscanf(fpr, "%2x", byte);
            //$display("%2x", byte);
            for(j = 0;j < 11;j = j + 1) begin
                if(j == 0) begin
                    rx = 0;
                end
                else if(j >= 1 && j <= 8) begin
                    rx = byte[0];
                    byte = byte >> 1;
                end
                else begin
                    rx = 1;
                end
                @(posedge rs_clk);
            end
        end
        $fclose(fpr);
        #2604000;
        $display("Timeout");
        $finish;
    end
    always @ (dbg_satisfy) begin
        if(dbg_satisfy) begin
            $display("Find");
            $display("%x", dbg_nonce - 48);
            $finish;
        end
    end
    /*
    always @ (tx) begin
        if(tx == 1'b1) begin
            $display("Find");
            #100000;
            $display("%x\n", dbg_header);
            $finish;
        end
    end
    */
endmodule
