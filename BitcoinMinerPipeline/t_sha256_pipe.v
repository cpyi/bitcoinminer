`timescale 10ns/10ps
module t_sha256_pipe();
    reg clk;
    reg rst;
    reg init;
    wire [511:0] chunk_1;
    wire [511:0] chunk_2;
    wire [255:0] output_data;
    wire [607:0] block_header;
    wire [31:0] nonce;
    wire output_valid;
    wire [7:0] cter;
    sha256_pipe sp0(.clk(clk), .rst(rst), .pipe_init(init), .chunk1(chunk_1), .chunk2(chunk_2), .output_data(output_data), .output_valid(output_valid));
    always #10 clk = ~clk;
    initial begin
        $dumpfile("sha256_pipe.vcd");
        $dumpvars;
    end
    assign nonce = 32'h9962E301;
    assign block_header = 608'h1d00ffff4966bc610e3e2357e806b6cdb1f70b54c3a3a17b6714ee1f0e68bebb44a74b1efd512098000000000019d6689c085ae165831e934ff763ae46a2a6c172b3f1b60a8ce26f00000001;
    assign chunk_1 = block_header[511:0];
    assign chunk_2 = {384'h800200000000000000000000000000000000000000000000000000000000000000000000000000000000000000000080, nonce, block_header[607:512]};
    initial begin
        clk = 0;
        rst = 0;
        init = 0;
        @(negedge clk);
            rst = 1;
        @(negedge clk);
            rst = 0;
            init = 1;
        @(negedge clk);
            init = 0;
        @(negedge clk);
        #10000;
        $finish;
    end
endmodule
