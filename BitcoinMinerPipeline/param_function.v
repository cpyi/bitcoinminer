`timescale 1ns / 1ps
module param_function(clk, rst, m0_in, m1_in, m2_in, m3_in, m4_in, m5_in, m6_in, m7_in, m8_in, m9_in, m10_in, m11_in, m12_in, m13_in, m14_in, m15_in, m0_out, m1_out, m2_out, m3_out, m4_out, m5_out, m6_out, m7_out, m8_out, m9_out, m10_out, m11_out, m12_out, m13_out, m14_out, m15_out, m0_reg, m1_reg, m2_reg, m3_reg, m4_reg, m5_reg, m6_reg, m7_reg, m8_reg, m9_reg, m10_reg, m11_reg, m12_reg, m13_reg,m14_reg, m15_reg);
    input clk;
    input rst;
    input [31:0] m0_in;
    input [31:0] m1_in;
    input [31:0] m2_in;
    input [31:0] m3_in;
    input [31:0] m4_in;
    input [31:0] m5_in;
    input [31:0] m6_in;
    input [31:0] m7_in;
    input [31:0] m8_in;
    input [31:0] m9_in;
    input [31:0] m10_in;
    input [31:0] m11_in;
    input [31:0] m12_in;
    input [31:0] m13_in;
    input [31:0] m14_in;
    input [31:0] m15_in;
    output reg [31:0] m0_reg;
    output reg [31:0] m1_reg;
    output reg [31:0] m2_reg;
    output reg [31:0] m3_reg;
    output reg [31:0] m4_reg;
    output reg [31:0] m5_reg;
    output reg [31:0] m6_reg;
    output reg [31:0] m7_reg;
    output reg [31:0] m8_reg;
    output reg [31:0] m9_reg;
    output reg [31:0] m10_reg;
    output reg [31:0] m11_reg;
    output reg [31:0] m12_reg;
    output reg [31:0] m13_reg;
    output reg [31:0] m14_reg;
    output reg [31:0] m15_reg;
    output reg [31:0] m0_out;
    output reg [31:0] m1_out;
    output reg [31:0] m2_out;
    output reg [31:0] m3_out;
    output reg [31:0] m4_out;
    output reg [31:0] m5_out;
    output reg [31:0] m6_out;
    output reg [31:0] m7_out;
    output reg [31:0] m8_out;
    output reg [31:0] m9_out;
    output reg [31:0] m10_out;
    output reg [31:0] m11_out;
    output reg [31:0] m12_out;
    output reg [31:0] m13_out;
    output reg [31:0] m14_out;
    output reg [31:0] m15_out;
    always @ (posedge clk) begin
        if(rst) begin
            m0_reg = 32'b0;
            m1_reg = 32'b0;
            m2_reg = 32'b0;
            m3_reg = 32'b0;
            m4_reg = 32'b0;
            m5_reg = 32'b0;
            m6_reg = 32'b0;
            m7_reg = 32'b0;
            m8_reg = 32'b0;
            m9_reg = 32'b0;
            m10_reg = 32'b0;
            m11_reg = 32'b0;
            m12_reg = 32'b0;
            m13_reg = 32'b0;
            m14_reg = 32'b0;
            m15_reg = 32'b0;
        end
        else begin
            m0_reg = m0_in;
            m1_reg = m1_in;
            m2_reg = m2_in;
            m3_reg = m3_in;
            m4_reg = m4_in;
            m5_reg = m5_in;
            m6_reg = m6_in;
            m7_reg = m7_in;
            m8_reg = m8_in;
            m9_reg = m9_in;
            m10_reg = m10_in;
            m11_reg = m11_in;
            m12_reg = m12_in;
            m13_reg = m13_in;
            m14_reg = m14_in;
            m15_reg = m15_in;
        end
    end
    always @ (*) begin
        m0_out = m1_reg;
        m1_out = m2_reg;
        m2_out = m3_reg;
        m3_out = m4_reg;
        m4_out = m5_reg;
        m5_out = m6_reg;
        m6_out = m7_reg;
        m7_out = m8_reg;
        m8_out = m9_reg;
        m9_out = m10_reg;
        m10_out = m11_reg;
        m11_out = m12_reg;
        m12_out = m13_reg;
        m13_out = m14_reg;
        m14_out = m15_reg;
        m15_out = (({m14_reg[16:0],m14_reg[31:17]}) ^ ({m14_reg[18:0],m14_reg[31:19]}) ^ (m14_reg >> 10)) + m9_reg + (({m1_reg[6:0],m1_reg[31:7]}) ^ ({m1_reg[17:0],m1_reg[31:18]}) ^ (m1_reg >> 3)) + m0_reg;
    end
endmodule
