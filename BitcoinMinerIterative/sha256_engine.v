`timescale 1ns / 1ps
module sha256_engine(clk, rst, chunk_data, chunk_ready, chunk_last, chunk_ack, output_data, output_valid, output_ack);
    input clk, rst;
    input [511:0] chunk_data;
    input chunk_ready, chunk_last;
    output reg chunk_ack;
    output reg [255:0] output_data;
    output reg output_valid;
    input output_ack;
    reg [511:0] chunk_reg;
    reg [511:0] chunk_reg_next;
    reg chunk_last_reg;
    reg chunk_last_reg_next;
    reg [2:0] current_state;
    reg [2:0] next_state;
    reg [6:0] loop_counter;
    reg [6:0] loop_counter_next;
    reg [31:0] a;
    reg [31:0] a_next;
    reg [31:0] b;
    reg [31:0] b_next;
    reg [31:0] c;
    reg [31:0] c_next;
    reg [31:0] d;
    reg [31:0] d_next;
    reg [31:0] e;
    reg [31:0] e_next;
    reg [31:0] f;
    reg [31:0] f_next;
    reg [31:0] g;
    reg [31:0] g_next;
    reg [31:0] h;
    reg [31:0] h_next;
    reg [31:0] a_saved;
    reg [31:0] b_saved;
    reg [31:0] c_saved;
    reg [31:0] d_saved;
    reg [31:0] e_saved;
    reg [31:0] f_saved;
    reg [31:0] g_saved;
    reg [31:0] h_saved;
    reg [31:0] a_saved_next;
    reg [31:0] b_saved_next;
    reg [31:0] c_saved_next;
    reg [31:0] d_saved_next;
    reg [31:0] e_saved_next;
    reg [31:0] f_saved_next;
    reg [31:0] g_saved_next;
    reg [31:0] h_saved_next;
    wire [31:0] a_out;
    wire [31:0] b_out;
    wire [31:0] c_out;
    wire [31:0] d_out;
    wire [31:0] e_out;
    wire [31:0] f_out;
    wire [31:0] g_out;
    wire [31:0] h_out;
    wire [31:0] k;
    wire [31:0] m;
    wire [31:0] m0;
    wire [31:0] m1;
    wire [31:0] m2;
    wire [31:0] m3;
    wire [31:0] m4;
    wire [31:0] m5;
    wire [31:0] m6;
    wire [31:0] m7;
    wire [31:0] m8;
    wire [31:0] m9;
    wire [31:0] m10;
    wire [31:0] m11;
    wire [31:0] m12;
    wire [31:0] m13;
    wire [31:0] m14;
    wire [31:0] m15;
    wire [31:0] m16;
    wire [31:0] m17;
    wire [31:0] m18;
    wire [31:0] m19;
    wire [31:0] m20;
    wire [31:0] m21;
    wire [31:0] m22;
    wire [31:0] m23;
    wire [31:0] m24;
    wire [31:0] m25;
    wire [31:0] m26;
    wire [31:0] m27;
    wire [31:0] m28;
    wire [31:0] m29;
    wire [31:0] m30;
    wire [31:0] m31;
    wire [31:0] m32;
    wire [31:0] m33;
    wire [31:0] m34;
    wire [31:0] m35;
    wire [31:0] m36;
    wire [31:0] m37;
    wire [31:0] m38;
    wire [31:0] m39;
    wire [31:0] m40;
    wire [31:0] m41;
    wire [31:0] m42;
    wire [31:0] m43;
    wire [31:0] m44;
    wire [31:0] m45;
    wire [31:0] m46;
    wire [31:0] m47;
    wire [31:0] m48;
    wire [31:0] m49;
    wire [31:0] m50;
    wire [31:0] m51;
    wire [31:0] m52;
    wire [31:0] m53;
    wire [31:0] m54;
    wire [31:0] m55;
    wire [31:0] m56;
    wire [31:0] m57;
    wire [31:0] m58;
    wire [31:0] m59;
    wire [31:0] m60;
    wire [31:0] m61;
    wire [31:0] m62;
    wire [31:0] m63;

    //Signal for m calculator
    reg m_clear;
    wire m_ready;
    
    //States
    parameter S_INIT = 3'd0;
    parameter S_COPY = 3'd1;
    parameter S_MDATA = 3'd2;
    parameter S_LOOP = 3'd3;
    parameter S_ADD = 3'd4;
    parameter S_VALID = 3'd5;
    parameter H0 = 32'h6a09e667;
    parameter H1 = 32'hbb67ae85;
    parameter H2 = 32'h3c6ef372;
    parameter H3 = 32'ha54ff53a;
    parameter H4 = 32'h510e527f;
    parameter H5 = 32'h9b05688c;
    parameter H6 = 32'h1f83d9ab;
    parameter H7 = 32'h5be0cd19;
    sha256_operation so0(.k(k), .m(m), .a_in(a), .b_in(b), .c_in(c), .d_in(d), .e_in(e), .f_in(f), .g_in(g), .h_in(h), .a_out(a_out), .b_out(b_out), .c_out(c_out), .d_out(d_out), .e_out(e_out), .f_out(f_out), .g_out(g_out), .h_out(h_out));
    k_selector ks0(.loop_counter(loop_counter), .k(k));
    m_calculator mc0(.clk(clk), .rst(rst), .clear(m_clear), .chunk_data(chunk_reg), .m0(m0), .m1(m1), .m2(m2), .m3(m3), .m4(m4), .m5(m5), .m6(m6), .m7(m7), .m8(m8), .m9(m9), .m10(m10), .m11(m11), .m12(m12), .m13(m13), .m14(m14), .m15(m15), .m16(m16), .m17(m17), .m18(m18), .m19(m19), .m20(m20), .m21(m21), .m22(m22), .m23(m23), .m24(m24), .m25(m25), .m26(m26), .m27(m27), .m28(m28), .m29(m29), .m30(m30), .m31(m31), .m32(m32), .m33(m33), .m34(m34), .m35(m35), .m36(m36), .m37(m37), .m38(m38), .m39(m39), .m40(m40), .m41(m41), .m42(m42), .m43(m43), .m44(m44), .m45(m45), .m46(m46), .m47(m47), .m48(m48), .m49(m49), .m50(m50), .m51(m51), .m52(m52), .m53(m53), .m54(m54), .m55(m55), .m56(m56), .m57(m57), .m58(m58), .m59(m59), .m60(m60), .m61(m61), .m62(m62), .m63(m63), .m_ready(m_ready));
    m_selector ms0(.loop_counter(loop_counter), .m0(m0), .m1(m1), .m2(m2), .m3(m3), .m4(m4), .m5(m5), .m6(m6), .m7(m7), .m8(m8), .m9(m9), .m10(m10), .m11(m11), .m12(m12), .m13(m13), .m14(m14), .m15(m15), .m16(m16), .m17(m17), .m18(m18), .m19(m19), .m20(m20), .m21(m21), .m22(m22), .m23(m23), .m24(m24), .m25(m25), .m26(m26), .m27(m27), .m28(m28), .m29(m29), .m30(m30), .m31(m31), .m32(m32), .m33(m33), .m34(m34), .m35(m35), .m36(m36), .m37(m37), .m38(m38), .m39(m39), .m40(m40), .m41(m41), .m42(m42), .m43(m43), .m44(m44), .m45(m45), .m46(m46), .m47(m47), .m48(m48), .m49(m49), .m50(m50), .m51(m51), .m52(m52), .m53(m53), .m54(m54), .m55(m55), .m56(m56), .m57(m57), .m58(m58), .m59(m59), .m60(m60), .m61(m61), .m62(m62), .m63(m63), .m(m));

    //States
    always @ (*) begin
        case(current_state)
            S_INIT: begin
                next_state = S_COPY;
            end
            S_COPY: begin
                if(chunk_ready)
                    next_state = S_MDATA;
                else
                    next_state = S_COPY;
            end
            S_MDATA: begin
                if(m_ready)
                    next_state = S_LOOP;
                else
                    next_state = S_MDATA;
            end
            S_LOOP: begin
                if(loop_counter == 7'd63)
                    next_state = S_ADD;
                else
                    next_state = S_LOOP;
            end
            S_ADD: begin
                if(chunk_last_reg)
                    next_state = S_VALID;
                else
                    next_state = S_COPY;
            end
            S_VALID: begin
                if(output_ack)
                    next_state = S_INIT;
                else
                    next_state = S_VALID;
            end
            default: begin
                next_state = S_INIT;
            end
        endcase
    end

    //chunk_reg
    always @ (*) begin
        if(current_state == S_INIT)
            chunk_reg_next = 512'b0;
        else if(current_state == S_COPY && chunk_ready)
            chunk_reg_next = chunk_data;
        else
            chunk_reg_next = chunk_reg;
    end

    //chunk_last_reg
    always @ (*) begin
        if(current_state == S_COPY && chunk_ready)
            chunk_last_reg_next = chunk_last;
        else
            chunk_last_reg_next = chunk_last_reg;
    end
    
    //loop counter
    always @ (*) begin
        if(current_state == S_INIT || current_state == S_COPY)
            loop_counter_next = 7'b0;
        else if(current_state == S_LOOP)
            loop_counter_next = loop_counter + 1'b1;
        else
            loop_counter_next = loop_counter;
    end

    //reg a~h
    always @ (*) begin
        if(current_state == S_INIT) begin
            a_next = 0;
            b_next = 0;
            c_next = 0;
            d_next = 0;
            e_next = 0;
            f_next = 0;
            g_next = 0;
            h_next = 0;
        end
        if(current_state == S_LOOP) begin
            a_next = a_out;
            b_next = b_out;
            c_next = c_out;
            d_next = d_out;
            e_next = e_out;
            f_next = f_out;
            g_next = g_out;
            h_next = h_out;
        end
        else if(current_state == S_COPY && chunk_ready) begin
            a_next = a_saved;
            b_next = b_saved;
            c_next = c_saved;
            d_next = d_saved;
            e_next = e_saved;
            f_next = f_saved;
            g_next = g_saved;
            h_next = h_saved;
        end
        else begin
            a_next = a;
            b_next = b;
            c_next = c;
            d_next = d;
            e_next = e;
            f_next = f;
            g_next = g;
            h_next = h;
        end
    end

    //a~h saved
    always @ (*) begin
        if(current_state == S_INIT) begin
            a_saved_next = H0;
            b_saved_next = H1;
            c_saved_next = H2;
            d_saved_next = H3;
            e_saved_next = H4;
            f_saved_next = H5;
            g_saved_next = H6;
            h_saved_next = H7;
        end
        else if(current_state == S_ADD) begin
            a_saved_next = a + a_saved;
            b_saved_next = b + b_saved;
            c_saved_next = c + c_saved;
            d_saved_next = d + d_saved;
            e_saved_next = e + e_saved;
            f_saved_next = f + f_saved;
            g_saved_next = g + g_saved;
            h_saved_next = h + h_saved;
        end
        else begin
            a_saved_next = a_saved;
            b_saved_next = b_saved;
            c_saved_next = c_saved;
            d_saved_next = d_saved;
            e_saved_next = e_saved;
            f_saved_next = f_saved;
            g_saved_next = g_saved;
            h_saved_next = h_saved;
        end
    end
    always @ (posedge clk) begin
        if(rst) begin
            current_state <= S_INIT;
            chunk_reg <= 512'b0;
            chunk_last_reg <= 1'b0;
            loop_counter <= 7'b0;
            a_saved <= H0;
            b_saved <= H1;
            c_saved <= H2;
            d_saved <= H3;
            e_saved <= H4;
            f_saved <= H5;
            g_saved <= H6;
            h_saved <= H7;
            a <= 0;
            b <= 0;
            c <= 0;
            d <= 0;
            e <= 0;
            f <= 0;
            g <= 0;
            h <= 0;
        end
        else begin
            current_state <= next_state;
            chunk_reg <= chunk_reg_next;
            chunk_last_reg <= chunk_last_reg_next;
            loop_counter <= loop_counter_next;
            a <= a_next;
            b <= b_next;
            c <= c_next;
            d <= d_next;
            e <= e_next;
            f <= f_next;
            g <= g_next;
            h <= h_next;
            a_saved <= a_saved_next;
            b_saved <= b_saved_next;
            c_saved <= c_saved_next;
            d_saved <= d_saved_next;
            e_saved <= e_saved_next;
            f_saved <= f_saved_next;
            g_saved <= g_saved_next;
            h_saved <= h_saved_next;
        end
    end
    always @ (*) begin
        case(current_state)
            S_INIT: begin
                output_valid = 1'b0;
                chunk_ack = 1'b0;
                output_data = 256'b0;
            end
            S_COPY: begin
                output_valid = 1'b0;
                if(chunk_ready)
                    chunk_ack = 1'b1;
                else
                    chunk_ack = 1'b0;
                output_data = 256'b0;
            end
            S_MDATA: begin
                output_valid = 1'b0;
                chunk_ack = 1'b0;
                output_data = 256'b0;
            end
            S_LOOP: begin
                output_valid = 1'b0;
                chunk_ack = 1'b0;
                output_data = 256'b0;
            end
            S_ADD: begin
                output_valid = 1'b0;
                chunk_ack = 1'b0;
                output_data = 256'b0;
            end
            S_VALID: begin
                output_valid = 1'b1;
                chunk_ack = 1'b0;
                output_data = {a_saved, b_saved, c_saved, d_saved, e_saved, f_saved, g_saved, h_saved};
            end
            default: begin
                output_valid = 1'b0;
                chunk_ack = 1'b0;
                output_data = 256'b0;
            end
        endcase
    end
    
    //Clear m calculator when copy state
    always @ (*) begin
        if(current_state == S_COPY)
            m_clear = 1'b1;
        else
            m_clear = 1'b0;
    end
endmodule
