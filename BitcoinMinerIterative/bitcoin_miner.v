`timescale 1ns / 1ps
module bitcoin_miner(clk, rst, rx, tx, dbg_nonce, dbg_header);
    //Port definition
    input clk;
    input rst;
    input rx;
    output tx;

    //Block header without nonce is 76 bytes, nonce is 4 bytes
    reg [607:0] block_header;
    reg [607:0] block_header_next;
    reg [31:0] nonce;
    reg [31:0] nonce_next;
    output [31:0] dbg_nonce;
    output [607:0] dbg_header;

    //In double hash stage1 require 2 chunks, state2 require 1 chunk
    wire [511:0] stage1_chunk1;
    wire [511:0] stage1_chunk2;
    wire [511:0] stage2_chunk1;
    reg chunk_ready;
    reg [511:0] chunk_data;
    reg chunk_last;
    wire chunk_ack;

    //SHA engine related port
    wire [255:0] sha_data;
    reg [255:0] sha_stage1_data;
    reg [255:0] sha_stage1_data_next;
    reg [255:0] sha_stage2_data;
    reg [255:0] sha_stage2_data_next;
    wire [255:0] hash_result;
    wire sha_valid;
    reg sha_ack;

    //FSM states
    parameter S_RX = 3'd0;
    parameter S_STAGE1_CHUNK1 = 3'd1;
    parameter S_STAGE1_CHUNK2 = 3'd2;
    parameter S_WAIT_STAGE1 = 3'd3;
    parameter S_STAGE2_CHUNK1 = 3'd4;
    parameter S_WAIT_STAGE2 = 3'd5;
    parameter S_CHECK_SATISFY = 3'd6;
    parameter S_TX = 3'd7;
    reg [2:0] current_state;
    reg [2:0] next_state;

    //RX received bytes counter
    reg [6:0] rx_count;
    reg [6:0] rx_count_next;

    //TX sended bytes counter
    reg [5:0] tx_count;
    reg [5:0] tx_count_next;

    //TX data
    reg tx_vld;
    reg [7:0] tx_data;
    reg [7:0] tx_byte;

    //RX data
    wire [7:0] rx_data;

    //Difficulty
    wire [31:0] difficulty;
    wire [7:0] diff_exp;
    wire [23:0] diff_val;
    wire [255:0] difficulty_expanded;
    wire satisfy;


    //RS232 module
    rs232 r0(.clk(clk), .rst(rst), .rx(rx), .tx_vld(tx_vld), .tx_data(tx_data), .rx_vld(rx_vld), .rx_data(rx_data), .tx(tx), .tx_rdy(tx_rdy));

    //SHA engine module
    sha256_engine se0(.clk(clk), .rst(rst), .chunk_data(chunk_data), .chunk_ready(chunk_ready), .chunk_last(chunk_last), .chunk_ack(chunk_ack), .output_data(sha_data), .output_valid(sha_valid), .output_ack(sha_ack));
    
    //States
    always @ (*) begin
        if(current_state == S_RX) begin
            if(rx_count == 7'd75 && rx_vld)
                next_state = S_STAGE1_CHUNK1;
            else
                next_state = S_RX;
        end
        else if(current_state == S_STAGE1_CHUNK1) begin
            if(!chunk_ack)
                next_state = S_STAGE1_CHUNK1;
            else
                next_state = S_STAGE1_CHUNK2;
        end
        else if(current_state == S_STAGE1_CHUNK2) begin
            if(!chunk_ack)
                next_state = S_STAGE1_CHUNK2;
            else
                next_state = S_WAIT_STAGE1;
        end
        else if(current_state == S_WAIT_STAGE1) begin
            if(!sha_valid)
                next_state = S_WAIT_STAGE1;
            else
                next_state = S_STAGE2_CHUNK1;
        end
        else if(current_state == S_STAGE2_CHUNK1) begin
            if(!chunk_ack)
                next_state = S_STAGE2_CHUNK1;
            else
                next_state = S_WAIT_STAGE2;
        end
        else if(current_state == S_WAIT_STAGE2) begin
            if(sha_valid)
                next_state = S_CHECK_SATISFY;
            else
                next_state = S_WAIT_STAGE2;
        end
        else if(current_state == S_CHECK_SATISFY) begin
            if(satisfy)
                next_state = S_TX;
            else if(!satisfy)
                next_state = S_STAGE1_CHUNK1;
        end
        else if(current_state == S_TX) begin
            if(tx_count == 31 && tx_rdy)
                next_state = S_RX;
            else
                next_state = S_TX;
        end
        else begin
            next_state = S_RX;
        end
    end

    //TX counter
    always @ (*) begin
        if(current_state == S_CHECK_SATISFY)
            tx_count_next = 6'b0;
        else if(current_state == S_TX && tx_rdy)
            tx_count_next = tx_count + 1;
        else
            tx_count_next = tx_count;
    end

    //RX counter
    always @ (*) begin
        if(current_state == S_TX) begin
            rx_count_next = 0;
        end
        else if(current_state == S_RX) begin
            if(rx_vld)
                rx_count_next = rx_count + 1;
            else
                rx_count_next = rx_count;
        end
        else begin
            rx_count_next = rx_count;
        end
    end

    //RX read in block header
    always @ (*) begin
        if(rx_vld)
            block_header_next = {rx_data, block_header[607:8]};
        else
            block_header_next = block_header;
    end

    //Chunk 1 and chunk 2 of stage 1
    assign stage1_chunk1 = block_header[511:0];
    assign stage1_chunk2 = {384'h800200000000000000000000000000000000000000000000000000000000000000000000000000000000000000000080, nonce, block_header[607:512]};

    //SHA stage1 output should be saved
    always @ (*) begin
        if(sha_valid && current_state == S_WAIT_STAGE1)
            sha_stage1_data_next = sha_data;
        else
            sha_stage1_data_next = sha_stage1_data;
    end

    //SHA stage2 output should be saved
    always @ (*) begin
        if(sha_valid && current_state == S_WAIT_STAGE2)
            sha_stage2_data_next = sha_data;
        else
            sha_stage2_data_next = sha_stage2_data;
    end

    //Hash result
    assign hash_result = {sha_stage2_data[7:0], sha_stage2_data[15:8], sha_stage2_data[23:16], sha_stage2_data[31:24], sha_stage2_data[39:32], sha_stage2_data[47:40], sha_stage2_data[55:48], sha_stage2_data[63:56], sha_stage2_data[71:64], sha_stage2_data[79:72], sha_stage2_data[87:80], sha_stage2_data[95:88], sha_stage2_data[103:96], sha_stage2_data[111:104], sha_stage2_data[119:112], sha_stage2_data[127:120], sha_stage2_data[135:128], sha_stage2_data[143:136], sha_stage2_data[151:144], sha_stage2_data[159:152], sha_stage2_data[167:160], sha_stage2_data[175:168], sha_stage2_data[183:176], sha_stage2_data[191:184], sha_stage2_data[199:192], sha_stage2_data[207:200], sha_stage2_data[215:208], sha_stage2_data[223:216], sha_stage2_data[231:224], sha_stage2_data[239:232], sha_stage2_data[247:240], sha_stage2_data[255:248]};
    

    //Chunk1 of stage2
    assign stage2_chunk1 = {256'h0001000000000000000000000000000000000000000000000000000000000080, sha_stage1_data[7:0], sha_stage1_data[15:8], sha_stage1_data[23:16], sha_stage1_data[31:24], sha_stage1_data[39:32], sha_stage1_data[47:40], sha_stage1_data[55:48], sha_stage1_data[63:56], sha_stage1_data[71:64], sha_stage1_data[79:72], sha_stage1_data[87:80], sha_stage1_data[95:88], sha_stage1_data[103:96], sha_stage1_data[111:104], sha_stage1_data[119:112], sha_stage1_data[127:120], sha_stage1_data[135:128], sha_stage1_data[143:136], sha_stage1_data[151:144], sha_stage1_data[159:152], sha_stage1_data[167:160], sha_stage1_data[175:168], sha_stage1_data[183:176], sha_stage1_data[191:184], sha_stage1_data[199:192], sha_stage1_data[207:200], sha_stage1_data[215:208], sha_stage1_data[223:216], sha_stage1_data[231:224], sha_stage1_data[239:232], sha_stage1_data[247:240], sha_stage1_data[255:248]};

    //Difficulty
    //assign difficulty = block_header[599:576];
    //assign difficulty = block_header[607:600];
    assign diff_val = block_header[599:576];
    assign diff_exp = block_header[607:600];
    //assign difficulty_expanded = 256'b0 | {difficulty, 192'b0};
    assign difficulty_expanded = 256'b0 | diff_val << ((diff_exp << 3) - 11'd24);
    assign satisfy = (hash_result < difficulty_expanded);

    //Nonce
    always @ (*) begin
        if(current_state == S_CHECK_SATISFY && satisfy)
            nonce_next = nonce;//nonce_next = 32'b0;
        else if(current_state == S_CHECK_SATISFY && !satisfy)
            nonce_next = nonce + 1;
        else
            nonce_next = nonce;
    end


    //Flip flop
    always @ (posedge clk) begin
        if(rst) begin
            block_header <= 608'b0;
            //nonce <= 32'b0;
            //nonce <= 32'd2504433960;
            nonce <= 32'd2573394670;
            sha_stage1_data <= 256'b0;
            sha_stage2_data <= 256'b0;
            current_state <= S_RX;
            rx_count <= 7'b0;
            tx_count <= 6'b0;
        end
        else begin
            block_header <= block_header_next;
            nonce <= nonce_next;
            sha_stage1_data <= sha_stage1_data_next;
            sha_stage2_data <= sha_stage2_data_next;
            current_state <= next_state;
            rx_count <= rx_count_next;
            tx_count <= tx_count_next;
        end
    end

    //Select chunk for SHA module
    always @ (*) begin
        case(current_state)
            S_RX: begin
                chunk_ready = 1'b0;
                chunk_data = 512'b0;
                chunk_last = 1'b0;
                sha_ack = 1'b0;
            end
            S_STAGE1_CHUNK1: begin
                chunk_ready = 1'b1;
                chunk_data = stage1_chunk1;
                chunk_last = 1'b0;
                sha_ack = 1'b0;
            end
            S_STAGE1_CHUNK2: begin
                chunk_ready = 1'b1;
                chunk_data = stage1_chunk2;
                chunk_last = 1'b1;
                sha_ack = 1'b0;
            end
            S_WAIT_STAGE1: begin
                chunk_ready = 1'b0;
                chunk_data = 512'b0;
                chunk_last = 1'b0;
                if(sha_valid)
                    sha_ack = 1'b1;
                else
                    sha_ack = 1'b0;
            end
            S_STAGE2_CHUNK1: begin
                chunk_ready = 1'b1;
                chunk_data = stage2_chunk1;
                chunk_last = 1'b1;
                sha_ack = 1'b0;
            end
            S_WAIT_STAGE2: begin
                chunk_ready = 1'b0;
                chunk_data = 512'b0;
                chunk_last = 1'b0;
                if(sha_valid)
                    sha_ack = 1'b1;
                else
                    sha_ack = 1'b0;
            end 
            S_CHECK_SATISFY: begin
                chunk_ready = 1'b0;
                chunk_data = 512'b0;
                chunk_last = 1'b0;
                sha_ack = 1'b0;
            end
            default: begin
                chunk_ready = 1'b0;
                chunk_data = 512'b0;
                chunk_last = 1'b0;
                sha_ack = 1'b0;
            end
        endcase
    end

    //Data for TX module
    always @ (*) begin
        case(current_state)
            S_RX: begin
                tx_vld = 1'b0;
                tx_data = 8'b0;
            end
            S_STAGE1_CHUNK1: begin
                tx_vld = 1'b0;
                tx_data = 8'b0;
            end
            S_STAGE1_CHUNK2: begin
                tx_vld = 1'b0;
                tx_data = 8'b0;
            end
            S_WAIT_STAGE1: begin
                tx_vld = 1'b0;
                tx_data = 8'b0;
            end
            S_STAGE2_CHUNK1: begin
                tx_vld = 1'b0;
                tx_data = 8'b0;
            end
            S_WAIT_STAGE2: begin
                tx_vld = 1'b0;
                tx_data = 8'b0;
            end 
            S_CHECK_SATISFY: begin
                tx_vld = 1'b0;
                tx_data = 8'b0;
            end
            S_TX: begin
                if(tx_rdy) begin
                    tx_vld = 1'b1;
                    tx_data = tx_byte;
                end
                else begin
                    tx_vld = 1'b0;
                    tx_data = 8'b0;
                end
            end
            default: begin
                tx_vld = 1'b0;
                tx_data = 8'b0;
            end
        endcase
    end

    //Byte to send
    always @ (*) begin
        case(tx_count)
            0: tx_byte = hash_result[7:0];
            1: tx_byte = hash_result[15:8];
            2: tx_byte = hash_result[23:16];
            3: tx_byte = hash_result[31:24];
            4: tx_byte = hash_result[39:32];
            5: tx_byte = hash_result[47:40];
            6: tx_byte = hash_result[55:48];
            7: tx_byte = hash_result[63:56];
            8: tx_byte = hash_result[71:64];
            9: tx_byte = hash_result[79:72];
            10: tx_byte = hash_result[87:80];
            11: tx_byte = hash_result[95:88];
            12: tx_byte = hash_result[103:96];
            13: tx_byte = hash_result[111:104];
            14: tx_byte = hash_result[119:112];
            15: tx_byte = hash_result[127:120];
            16: tx_byte = hash_result[135:128];
            17: tx_byte = hash_result[143:136];
            18: tx_byte = hash_result[151:144];
            19: tx_byte = hash_result[159:152];
            20: tx_byte = hash_result[167:160];
            21: tx_byte = hash_result[175:168];
            22: tx_byte = hash_result[183:176];
            23: tx_byte = hash_result[191:184];
            24: tx_byte = hash_result[199:192];
            25: tx_byte = hash_result[207:200];
            26: tx_byte = hash_result[215:208];
            27: tx_byte = hash_result[223:216];
            28: tx_byte = hash_result[231:224];
            29: tx_byte = hash_result[239:232];
            30: tx_byte = hash_result[247:240];
            31: tx_byte = hash_result[255:248];
            default: tx_byte = 8'b0;
        endcase
    end
    assign dbg_nonce = nonce;
    assign dbg_header = block_header;
endmodule
