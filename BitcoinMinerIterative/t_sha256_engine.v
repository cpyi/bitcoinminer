`timescale 10ns/10ps
module t_sha256_engine();
    reg clk;
    reg rst;
    reg [511:0] chunk_data;
    wire chunk_ack;
    wire [255:0] output_data;
    wire output_valid;
    integer i;
    sha256_engine se0(.clk(clk), .rst(rst), .chunk_data(chunk_data), .chunk_ready(1'b1), .chunk_last(1'b0), .chunk_ack(chunk_ack), .output_data(output_data), .output_valid(output_valid), .output_ack(1'b0));
    always #10 clk = ~clk;
    initial begin
        $dumpfile("sha256_engine.vcd");
        $dumpvars;
    end
    initial begin
        clk = 0;
        rst = 0;
      //chunk_data = 512'H00000000000000640000000000000000000000000000000000000001636261605f5e5d5c5b5a595857565554535251504f4e4d4c4b4a49484746454443424140;
        //chunk_data = 512'H08000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000008041;
        //chunk_data = 512'H10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000804241;
        //chunk_data = 512'H00000000000000080000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000141;
        chunk_data = 512'H08000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000008061;

        @(negedge clk);
            rst = 1;
        @(negedge clk);
        rst = 0;
        for(i = 0;i < 140;i = i + 1) begin
            @(negedge clk);
            if(output_data != 256'b0) begin
                $display("%h", output_data);
                chunk_data = 512'HB002000000000000000000000000000000000000000000000000000000000000000000000000000000804C4B4A4C4B4A4C4A4C4B4B4A4C4B4A4C4A4C4B4A4C4B;
            end
        end
        $finish;
    end
endmodule
