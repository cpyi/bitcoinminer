`timescale 10ns/10ps
module t_input_interface();

    //Port definition
    reg clk, rst, input_valid, input_last;
    reg [7:0] input_byte;
    reg [7:0] next_byte;
    wire input_ready, chunk_ready, chunk_last, chunk_ack;
    wire [511:0] chunk_data;
    wire [255:0] output_data;

    //Misc vars
    reg cnt;
    integer fp;
    reg end_loop;

    //Module
    input_interface ii(.clk(clk), .rst(rst), .input_byte(input_byte), .input_valid(input_valid), .input_last(input_last), .input_ready(input_ready), .chunk_data(chunk_data), .chunk_ready(chunk_ready), .chunk_last(chunk_last), .chunk_ack(chunk_ack));
    sha256_engine se0(.clk(clk), .rst(rst), .chunk_data(chunk_data), .chunk_ready(chunk_ready), .chunk_last(chunk_last), .chunk_ack(chunk_ack), .output_data(output_data), .output_valid(output_valid), .output_ack(1'b0));

    always #10 clk = ~clk;
    initial begin
        fp = $fopen("sha256_whole_data.in", "r");
        $dumpfile("sha256_whole_data.vcd");
        $dumpvars;

        //Initialization of registers
        clk = 0;
        rst = 0;
        input_byte = 0;
        input_valid = 0;
        input_last = 0;
        end_loop = 0;

        //Reset
        @(negedge clk);
        rst = 1;
        @(negedge clk);
        rst = 0;
        @(negedge clk);

        //Input First
        //cnt = $fscanf(fp, "%2x", next_byte);
        while(!end_loop) begin
            if(input_ready) begin
                //input_byte = next_byte;
                cnt = $fscanf(fp, "%2x ", input_byte);
                //$display("NB: %02x", input_byte);
                if(!$feof(fp)) begin
                    input_last = 0;
                    input_valid = 1;
                end
                else begin
                    input_last = 1;
                    end_loop = 1;
                    input_valid = 1;
                end
                //$display("input_byte: %c, input_last: %b", input_byte, input_last);
            end
            else begin
                input_byte = 0;
                input_last = 0;
            end
            @(negedge clk);
        end
        @(negedge clk);
            input_valid = 0;
        @(negedge clk);
        #20000;
        $fclose(fp);
        $finish;
    end
    //Check chunk data
    always @ (posedge chunk_ready) begin
        $display("cdta: %x\n", chunk_data);
    end
    //Check output data
    always @ (posedge output_valid) begin
        $display("odta: %x\n", output_data);
    end
endmodule
