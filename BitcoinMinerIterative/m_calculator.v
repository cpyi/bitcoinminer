module m_calculator(clk, rst, clear, chunk_data, m0, m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12, m13, m14, m15, m16, m17, m18, m19, m20, m21, m22, m23, m24, m25, m26, m27, m28, m29, m30, m31, m32, m33, m34, m35, m36, m37, m38, m39, m40, m41, m42, m43, m44, m45, m46, m47, m48, m49, m50, m51, m52, m53, m54, m55, m56, m57, m58, m59, m60, m61, m62, m63, m_ready);
    input clk;
    input rst;
    input clear;
    input [511:0] chunk_data;
    output [31:0] m0;
    output [31:0] m1;
    output [31:0] m2;
    output [31:0] m3;
    output [31:0] m4;
    output [31:0] m5;
    output [31:0] m6;
    output [31:0] m7;
    output [31:0] m8;
    output [31:0] m9;
    output [31:0] m10;
    output [31:0] m11;
    output [31:0] m12;
    output [31:0] m13;
    output [31:0] m14;
    output [31:0] m15;
    output reg [31:0] m16;
    output reg [31:0] m17;
    output reg [31:0] m18;
    output reg [31:0] m19;
    output reg [31:0] m20;
    output reg [31:0] m21;
    output reg [31:0] m22;
    output reg [31:0] m23;
    output reg [31:0] m24;
    output reg [31:0] m25;
    output reg [31:0] m26;
    output reg [31:0] m27;
    output reg [31:0] m28;
    output reg [31:0] m29;
    output reg [31:0] m30;
    output reg [31:0] m31;
    output reg [31:0] m32;
    output reg [31:0] m33;
    output reg [31:0] m34;
    output reg [31:0] m35;
    output reg [31:0] m36;
    output reg [31:0] m37;
    output reg [31:0] m38;
    output reg [31:0] m39;
    output reg [31:0] m40;
    output reg [31:0] m41;
    output reg [31:0] m42;
    output reg [31:0] m43;
    output reg [31:0] m44;
    output reg [31:0] m45;
    output reg [31:0] m46;
    output reg [31:0] m47;
    output reg [31:0] m48;
    output reg [31:0] m49;
    output reg [31:0] m50;
    output reg [31:0] m51;
    output reg [31:0] m52;
    output reg [31:0] m53;
    output reg [31:0] m54;
    output reg [31:0] m55;
    output reg [31:0] m56;
    output reg [31:0] m57;
    output reg [31:0] m58;
    output reg [31:0] m59;
    output reg [31:0] m60;
    output reg [31:0] m61;
    output reg [31:0] m62;
    output reg [31:0] m63;
    output m_ready;
    
    //Signal for m calculation
    wire [31:0] m_rslt;
    reg [31:0] m_op_a;
    reg [31:0] m_op_b;
    reg [31:0] m_op_c;
    reg [31:0] m_op_d;

    //Loop counter
    reg [5:0] loop_counter;
   
    //Calculation of m0~m15 is simple
    assign m0 = {chunk_data[7:0], chunk_data[15:8], chunk_data[23:16], chunk_data[31:24]};
    assign m1 = {chunk_data[39:32], chunk_data[47:40], chunk_data[55:48], chunk_data[63:56]};
    assign m2 = {chunk_data[71:64], chunk_data[79:72], chunk_data[87:80], chunk_data[95:88]};
    assign m3 = {chunk_data[103:96], chunk_data[111:104], chunk_data[119:112], chunk_data[127:120]};
    assign m4 = {chunk_data[135:128], chunk_data[143:136], chunk_data[151:144], chunk_data[159:152]};
    assign m5 = {chunk_data[167:160], chunk_data[175:168], chunk_data[183:176], chunk_data[191:184]};
    assign m6 = {chunk_data[199:192], chunk_data[207:200], chunk_data[215:208], chunk_data[223:216]};
    assign m7 = {chunk_data[231:224], chunk_data[239:232], chunk_data[247:240], chunk_data[255:248]};
    assign m8 = {chunk_data[263:256], chunk_data[271:264], chunk_data[279:272], chunk_data[287:280]};
    assign m9 = {chunk_data[295:288], chunk_data[303:296], chunk_data[311:304], chunk_data[319:312]};
    assign m10 = {chunk_data[327:320], chunk_data[335:328], chunk_data[343:336], chunk_data[351:344]};
    assign m11 = {chunk_data[359:352], chunk_data[367:360], chunk_data[375:368], chunk_data[383:376]};
    assign m12 = {chunk_data[391:384], chunk_data[399:392], chunk_data[407:400], chunk_data[415:408]};
    assign m13 = {chunk_data[423:416], chunk_data[431:424], chunk_data[439:432], chunk_data[447:440]};
    assign m14 = {chunk_data[455:448], chunk_data[463:456], chunk_data[471:464], chunk_data[479:472]};
    assign m15 = {chunk_data[487:480], chunk_data[495:488], chunk_data[503:496], chunk_data[511:504]};

    //Operation at each clock
    assign m_rslt = (({m_op_a[16:0],m_op_a[31:17]}) ^ ({m_op_a[18:0],m_op_a[31:19]}) ^ (m_op_a >> 10)) + m_op_b + (({m_op_c[6:0],m_op_c[31:7]}) ^ ({m_op_c[17:0],m_op_c[31:18]}) ^ (m_op_c >> 3)) + m_op_d;

    //Loop counter
    always @ (posedge clk) begin
        if(rst | clear)
            loop_counter <= 6'b0;
        else if(loop_counter < 48)
            loop_counter <= loop_counter + 1;
        else
            loop_counter <= loop_counter;
    end
    
    //Select input for the operation
    always @ (*) begin
        case(loop_counter)
            0: m_op_a = m14;
            1: m_op_a = m15;
            2: m_op_a = m16;
            3: m_op_a = m17;
            4: m_op_a = m18;
            5: m_op_a = m19;
            6: m_op_a = m20;
            7: m_op_a = m21;
            8: m_op_a = m22;
            9: m_op_a = m23;
            10: m_op_a = m24;
            11: m_op_a = m25;
            12: m_op_a = m26;
            13: m_op_a = m27;
            14: m_op_a = m28;
            15: m_op_a = m29;
            16: m_op_a = m30;
            17: m_op_a = m31;
            18: m_op_a = m32;
            19: m_op_a = m33;
            20: m_op_a = m34;
            21: m_op_a = m35;
            22: m_op_a = m36;
            23: m_op_a = m37;
            24: m_op_a = m38;
            25: m_op_a = m39;
            26: m_op_a = m40;
            27: m_op_a = m41;
            28: m_op_a = m42;
            29: m_op_a = m43;
            30: m_op_a = m44;
            31: m_op_a = m45;
            32: m_op_a = m46;
            33: m_op_a = m47;
            34: m_op_a = m48;
            35: m_op_a = m49;
            36: m_op_a = m50;
            37: m_op_a = m51;
            38: m_op_a = m52;
            39: m_op_a = m53;
            40: m_op_a = m54;
            41: m_op_a = m55;
            42: m_op_a = m56;
            43: m_op_a = m57;
            44: m_op_a = m58;
            45: m_op_a = m59;
            46: m_op_a = m60;
            47: m_op_a = m61;
            default: m_op_a = 32'b0;
        endcase
    end
    always @ (*) begin
        case(loop_counter)
            0: m_op_b = m9;
            1: m_op_b = m10;
            2: m_op_b = m11;
            3: m_op_b = m12;
            4: m_op_b = m13;
            5: m_op_b = m14;
            6: m_op_b = m15;
            7: m_op_b = m16;
            8: m_op_b = m17;
            9: m_op_b = m18;
            10: m_op_b = m19;
            11: m_op_b = m20;
            12: m_op_b = m21;
            13: m_op_b = m22;
            14: m_op_b = m23;
            15: m_op_b = m24;
            16: m_op_b = m25;
            17: m_op_b = m26;
            18: m_op_b = m27;
            19: m_op_b = m28;
            20: m_op_b = m29;
            21: m_op_b = m30;
            22: m_op_b = m31;
            23: m_op_b = m32;
            24: m_op_b = m33;
            25: m_op_b = m34;
            26: m_op_b = m35;
            27: m_op_b = m36;
            28: m_op_b = m37;
            29: m_op_b = m38;
            30: m_op_b = m39;
            31: m_op_b = m40;
            32: m_op_b = m41;
            33: m_op_b = m42;
            34: m_op_b = m43;
            35: m_op_b = m44;
            36: m_op_b = m45;
            37: m_op_b = m46;
            38: m_op_b = m47;
            39: m_op_b = m48;
            40: m_op_b = m49;
            41: m_op_b = m50;
            42: m_op_b = m51;
            43: m_op_b = m52;
            44: m_op_b = m53;
            45: m_op_b = m54;
            46: m_op_b = m55;
            47: m_op_b = m56;
            default: m_op_b = 32'b0;
        endcase
    end
    always @ (*) begin
        case(loop_counter)
            0: m_op_c = m1;
            1: m_op_c = m2;
            2: m_op_c = m3;
            3: m_op_c = m4;
            4: m_op_c = m5;
            5: m_op_c = m6;
            6: m_op_c = m7;
            7: m_op_c = m8;
            8: m_op_c = m9;
            9: m_op_c = m10;
            10: m_op_c = m11;
            11: m_op_c = m12;
            12: m_op_c = m13;
            13: m_op_c = m14;
            14: m_op_c = m15;
            15: m_op_c = m16;
            16: m_op_c = m17;
            17: m_op_c = m18;
            18: m_op_c = m19;
            19: m_op_c = m20;
            20: m_op_c = m21;
            21: m_op_c = m22;
            22: m_op_c = m23;
            23: m_op_c = m24;
            24: m_op_c = m25;
            25: m_op_c = m26;
            26: m_op_c = m27;
            27: m_op_c = m28;
            28: m_op_c = m29;
            29: m_op_c = m30;
            30: m_op_c = m31;
            31: m_op_c = m32;
            32: m_op_c = m33;
            33: m_op_c = m34;
            34: m_op_c = m35;
            35: m_op_c = m36;
            36: m_op_c = m37;
            37: m_op_c = m38;
            38: m_op_c = m39;
            39: m_op_c = m40;
            40: m_op_c = m41;
            41: m_op_c = m42;
            42: m_op_c = m43;
            43: m_op_c = m44;
            44: m_op_c = m45;
            45: m_op_c = m46;
            46: m_op_c = m47;
            47: m_op_c = m48;
            default: m_op_c = 32'b0;
        endcase
    end
    always @ (*) begin
        case(loop_counter)
            0: m_op_d = m0;
            1: m_op_d = m1;
            2: m_op_d = m2;
            3: m_op_d = m3;
            4: m_op_d = m4;
            5: m_op_d = m5;
            6: m_op_d = m6;
            7: m_op_d = m7;
            8: m_op_d = m8;
            9: m_op_d = m9;
            10: m_op_d = m10;
            11: m_op_d = m11;
            12: m_op_d = m12;
            13: m_op_d = m13;
            14: m_op_d = m14;
            15: m_op_d = m15;
            16: m_op_d = m16;
            17: m_op_d = m17;
            18: m_op_d = m18;
            19: m_op_d = m19;
            20: m_op_d = m20;
            21: m_op_d = m21;
            22: m_op_d = m22;
            23: m_op_d = m23;
            24: m_op_d = m24;
            25: m_op_d = m25;
            26: m_op_d = m26;
            27: m_op_d = m27;
            28: m_op_d = m28;
            29: m_op_d = m29;
            30: m_op_d = m30;
            31: m_op_d = m31;
            32: m_op_d = m32;
            33: m_op_d = m33;
            34: m_op_d = m34;
            35: m_op_d = m35;
            36: m_op_d = m36;
            37: m_op_d = m37;
            38: m_op_d = m38;
            39: m_op_d = m39;
            40: m_op_d = m40;
            41: m_op_d = m41;
            42: m_op_d = m42;
            43: m_op_d = m43;
            44: m_op_d = m44;
            45: m_op_d = m45;
            46: m_op_d = m46;
            47: m_op_d = m47;
            default: m_op_d = 32'b0;
        endcase
    end

    //Maintain registers
    always @ (posedge clk) begin
        if(rst | clear)
            m16 <= 0;
        else if(loop_counter == 0)
            m16 <= m_rslt;
        else
            m16 <= m16;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m17 <= 0;
        else if(loop_counter == 1)
            m17 <= m_rslt;
        else
            m17 <= m17;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m18 <= 0;
        else if(loop_counter == 2)
            m18 <= m_rslt;
        else
            m18 <= m18;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m19 <= 0;
        else if(loop_counter == 3)
            m19 <= m_rslt;
        else
            m19 <= m19;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m20 <= 0;
        else if(loop_counter == 4)
            m20 <= m_rslt;
        else
            m20 <= m20;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m21 <= 0;
        else if(loop_counter == 5)
            m21 <= m_rslt;
        else
            m21 <= m21;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m22 <= 0;
        else if(loop_counter == 6)
            m22 <= m_rslt;
        else
            m22 <= m22;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m23 <= 0;
        else if(loop_counter == 7)
            m23 <= m_rslt;
        else
            m23 <= m23;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m24 <= 0;
        else if(loop_counter == 8)
            m24 <= m_rslt;
        else
            m24 <= m24;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m25 <= 0;
        else if(loop_counter == 9)
            m25 <= m_rslt;
        else
            m25 <= m25;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m26 <= 0;
        else if(loop_counter == 10)
            m26 <= m_rslt;
        else
            m26 <= m26;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m27 <= 0;
        else if(loop_counter == 11)
            m27 <= m_rslt;
        else
            m27 <= m27;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m28 <= 0;
        else if(loop_counter == 12)
            m28 <= m_rslt;
        else
            m28 <= m28;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m29 <= 0;
        else if(loop_counter == 13)
            m29 <= m_rslt;
        else
            m29 <= m29;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m30 <= 0;
        else if(loop_counter == 14)
            m30 <= m_rslt;
        else
            m30 <= m30;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m31 <= 0;
        else if(loop_counter == 15)
            m31 <= m_rslt;
        else
            m31 <= m31;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m32 <= 0;
        else if(loop_counter == 16)
            m32 <= m_rslt;
        else
            m32 <= m32;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m33 <= 0;
        else if(loop_counter == 17)
            m33 <= m_rslt;
        else
            m33 <= m33;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m34 <= 0;
        else if(loop_counter == 18)
            m34 <= m_rslt;
        else
            m34 <= m34;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m35 <= 0;
        else if(loop_counter == 19)
            m35 <= m_rslt;
        else
            m35 <= m35;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m36 <= 0;
        else if(loop_counter == 20)
            m36 <= m_rslt;
        else
            m36 <= m36;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m37 <= 0;
        else if(loop_counter == 21)
            m37 <= m_rslt;
        else
            m37 <= m37;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m38 <= 0;
        else if(loop_counter == 22)
            m38 <= m_rslt;
        else
            m38 <= m38;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m39 <= 0;
        else if(loop_counter == 23)
            m39 <= m_rslt;
        else
            m39 <= m39;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m40 <= 0;
        else if(loop_counter == 24)
            m40 <= m_rslt;
        else
            m40 <= m40;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m41 <= 0;
        else if(loop_counter == 25)
            m41 <= m_rslt;
        else
            m41 <= m41;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m42 <= 0;
        else if(loop_counter == 26)
            m42 <= m_rslt;
        else
            m42 <= m42;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m43 <= 0;
        else if(loop_counter == 27)
            m43 <= m_rslt;
        else
            m43 <= m43;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m44 <= 0;
        else if(loop_counter == 28)
            m44 <= m_rslt;
        else
            m44 <= m44;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m45 <= 0;
        else if(loop_counter == 29)
            m45 <= m_rslt;
        else
            m45 <= m45;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m46 <= 0;
        else if(loop_counter == 30)
            m46 <= m_rslt;
        else
            m46 <= m46;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m47 <= 0;
        else if(loop_counter == 31)
            m47 <= m_rslt;
        else
            m47 <= m47;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m48 <= 0;
        else if(loop_counter == 32)
            m48 <= m_rslt;
        else
            m48 <= m48;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m49 <= 0;
        else if(loop_counter == 33)
            m49 <= m_rslt;
        else
            m49 <= m49;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m50 <= 0;
        else if(loop_counter == 34)
            m50 <= m_rslt;
        else
            m50 <= m50;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m51 <= 0;
        else if(loop_counter == 35)
            m51 <= m_rslt;
        else
            m51 <= m51;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m52 <= 0;
        else if(loop_counter == 36)
            m52 <= m_rslt;
        else
            m52 <= m52;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m53 <= 0;
        else if(loop_counter == 37)
            m53 <= m_rslt;
        else
            m53 <= m53;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m54 <= 0;
        else if(loop_counter == 38)
            m54 <= m_rslt;
        else
            m54 <= m54;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m55 <= 0;
        else if(loop_counter == 39)
            m55 <= m_rslt;
        else
            m55 <= m55;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m56 <= 0;
        else if(loop_counter == 40)
            m56 <= m_rslt;
        else
            m56 <= m56;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m57 <= 0;
        else if(loop_counter == 41)
            m57 <= m_rslt;
        else
            m57 <= m57;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m58 <= 0;
        else if(loop_counter == 42)
            m58 <= m_rslt;
        else
            m58 <= m58;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m59 <= 0;
        else if(loop_counter == 43)
            m59 <= m_rslt;
        else
            m59 <= m59;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m60 <= 0;
        else if(loop_counter == 44)
            m60 <= m_rslt;
        else
            m60 <= m60;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m61 <= 0;
        else if(loop_counter == 45)
            m61 <= m_rslt;
        else
            m61 <= m61;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m62 <= 0;
        else if(loop_counter == 46)
            m62 <= m_rslt;
        else
            m62 <= m62;
    end
    always @ (posedge clk) begin
        if(rst | clear)
            m63 <= 0;
        else if(loop_counter == 47)
            m63 <= m_rslt;
        else
            m63 <= m63;
    end
    assign m_ready = (loop_counter == 6'd48);
endmodule
