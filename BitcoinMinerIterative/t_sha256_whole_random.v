`timescale 10ns/10ps
module t_input_interface();

    //Port definition
    reg clk, rst, input_valid, input_last;
    reg [7:0] input_byte;
    reg [7:0] next_byte;
    reg output_ack;
    wire input_ready, chunk_ready, chunk_last, chunk_ack;
    wire [511:0] chunk_data;
    wire [255:0] output_data;
    //Misc vars
    integer in_file, out_file, i, j, k;
    reg [1000*8-1:0] string;
    integer return;
    reg end_loop;

    //Module
    input_interface ii(.clk(clk), .rst(rst), .input_byte(input_byte), .input_valid(input_valid), .input_last(input_last), .input_ready(input_ready), .chunk_data(chunk_data), .chunk_ready(chunk_ready), .chunk_last(chunk_last), .chunk_ack(chunk_ack));
    sha256_engine se0(.clk(clk), .rst(rst), .chunk_data(chunk_data), .chunk_ready(chunk_ready), .chunk_last(chunk_last), .chunk_ack(chunk_ack), .output_data(output_data), .output_valid(output_valid), .output_ack(output_ack));

    always #10 clk = ~clk;
    initial begin
        $dumpfile("sha256_whole_random.vcd");
        $dumpvars;
    end
    initial begin
        in_file = $fopen("sha256_whole_random.in", "r");
        out_file = $fopen("sha256_whole_random.out", "r");
        //Initialization of registers
        clk = 0;
        rst = 0;
        input_byte = 0;
        input_valid = 0;
        input_last = 0;
        end_loop = 0;
        k = 1;
        output_ack = 0;

        //Reset
        @(negedge clk);
        rst = 1;
        @(negedge clk);
        rst = 0;
        @(negedge clk);

        //Input
        for(i = 1;i <= 1000;i = i + 1) begin
            return = $fscanf(in_file, "%s", string);
            //return = $fgets(string, in_file);
            //$display("sssssssssss:%s", string);
            j = 0;
            end_loop = 0;
            while(!end_loop) begin
                if(input_ready) begin
                    input_byte = string[8*(i-1)-8*j +:7];
                    input_valid = 1;
                    if(j == i-1) begin
                        input_last = 1;
                        end_loop = 1;
                    end
                    else begin
                        input_last = 0;
                    end
                    j = j + 1;
                    //$display("input_byte: %c, input_last: %b", input_byte, input_last);
                end
                else begin
                    input_valid = 0;
                end
                @(negedge clk);
            end
            input_valid = 0;
        end
        #20000;
        $fclose(in_file);
        $fclose(out_file);
        $finish;
    end
    //Check chunk data
    /*
    always @ (posedge chunk_ready) begin
        $display("cdta: %x\n", chunk_data);
    end
    */
    //Check output data
    always @ (posedge output_valid) begin
        $display("%x", output_data);
        k = k + 1;
        @(negedge clk);
        output_ack = 1;
        @(negedge clk);
        output_ack = 0;
    end
endmodule
