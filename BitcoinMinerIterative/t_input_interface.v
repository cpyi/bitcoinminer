`timescale 10ns/10ps
module t_input_interface();
    reg clk, rst, input_valid, input_last;
    reg [7:0] input_byte;
    reg [7:0] next_byte;
    reg end_loop;
    wire input_ready, chunk_ready, chunk_last;
    wire [511:0] chunk_data;
    reg chunk_ack, cnt;
    integer fp;
    input_interface ii(.clk(clk), .rst(rst), .input_byte(input_byte), .input_valid(input_valid), .input_last(input_last), .input_ready(input_ready), .chunk_data(chunk_data), .chunk_ready(chunk_ready), .chunk_last(chunk_last), .chunk_ack(chunk_ack));
    always #10 clk = ~clk;
    initial begin
        fp = $fopen("data_in.txt", "r");
        $dumpfile("input_interface.vcd");
        $dumpvars;

        //Initialization of registers
        clk = 0;
        rst = 0;
        input_byte = 0;
        input_valid = 0;
        input_last = 0;
        chunk_ack = 0;
        end_loop = 0;

        //Reset
        @(negedge clk);
        rst = 1;
        @(negedge clk);
        rst = 0;
        @(negedge clk);

        //Input First
        cnt = $fscanf(fp, "%c", next_byte);
        while(!end_loop) begin
            if(input_ready) begin
                input_byte = next_byte;
                cnt = $fscanf(fp, "%c", next_byte);
                if((next_byte >= 65 && next_byte <= 90) || (next_byte >= 97 && next_byte <= 122)) begin
                    input_last = 0;
                    input_valid = 1;
                end
                else begin
                    input_last = 1;
                    end_loop = 1;
                    input_valid = 1;
                end
                //$display("input_byte: %c, input_last: %b", input_byte, input_last);
            end
            else begin
                input_byte = 0;
                input_last = 0;
            end
            @(negedge clk);
        end
        @(negedge clk);
            input_valid = 0;
        @(negedge clk);
        #100;
        $fclose(fp);
        $finish;
    end
    always @ (posedge chunk_ready) begin
        //$display("chunk_data: %x", chunk_data);
        $display("%x", chunk_data);
        #50;
        @(negedge clk);
            chunk_ack = 1;
        @(negedge clk);
            chunk_ack = 0;
    end
endmodule
