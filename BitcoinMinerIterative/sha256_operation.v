`timescale 1ns / 1ps
module sha256_operation(k, m, a_in, b_in, c_in, d_in, e_in, f_in, g_in,h_in, a_out, b_out, c_out, d_out, e_out, f_out, g_out, h_out);
    input [31:0] k, m, a_in, b_in, c_in, d_in, e_in, f_in, g_in, h_in;
    output reg [31:0] a_out, b_out, c_out, d_out, e_out, f_out, g_out, h_out;
    reg [31:0] ch, maj, s0, s1;
    always @ (*) begin
        ch = ((e_in & f_in) ^ ((~e_in) & g_in));
        maj = ((a_in & b_in) ^ (a_in & c_in) ^ (b_in & c_in));
        s0 = ({a_in[1:0],a_in[31:2]}) ^ ({a_in[12:0],a_in[31:13]}) ^ ({a_in[21:0],a_in[31:22]});
        s1 = ({e_in[5:0],e_in[31:6]}) ^ ({e_in[10:0],e_in[31:11]}) ^ ({e_in[24:0],e_in[31:25]});
        a_out = h_in + s1 + ch + k + m + s0 + maj;
        b_out = a_in;
        c_out = b_in;
        d_out = c_in;
        e_out = d_in + h_in + s1 + ch + k + m;
        f_out = e_in;
        g_out = f_in;
        h_out = g_in;
    end
endmodule
