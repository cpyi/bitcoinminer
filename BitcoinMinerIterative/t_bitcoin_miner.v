`timescale 10ns/10ps
module t_bitcoin_miner();

    //Port definition
    reg clk, rst, rx, rs_clk;
    reg [7:0] byte;
    wire tx;
    wire [31:0] dbg_nonce;
    wire [607:0] dbg_header;

    //Misc vars
    integer i, j, fpr, r, fpw_rst, fpw_rx, rrst, rrx;

    //Module
    bitcoin_miner bm0(.clk(clk), .rst(rst), .rx(rx), .tx(tx), .dbg_nonce(dbg_nonce), .dbg_header(dbg_header));

    //Clock generation
    always #10 clk = ~clk;
    always #50000 rs_clk = ~rs_clk;
    //Dump file
    initial begin
        $dumpfile("bitcoin_miner.vcd");
        $dumpvars;
    end

    //Monitor
    initial begin
        $monitor(dbg_nonce);
        if(dbg_nonce == 32'd2504433987) begin
            $display("!!!!!!!!!!!!!!");
            $finish;
        end
    end

    initial begin

        //Open file
        fpr = $fopen("bitcoin_miner.in", "r");
        fpw_rst = $fopen("rst.sig", "w");
        fpw_rx = $fopen("rx.sig", "w");

        //Initialization of registers
        clk = 0;
        rs_clk = 0;
        rst = 0;
        rx = 1;

        //Reset
        @(negedge clk);
        rst = 1;
        @(negedge clk);
        rst = 0;
        @(negedge clk);
        @(posedge rs_clk);
        //Loop
        for(i = 0;i < 76;i = i + 1) begin
            r = $fscanf(fpr, "%2x", byte);
            //$display("%2x", byte);
            for(j = 0;j < 11;j = j + 1) begin
                if(j == 0) begin
                    rx = 0;
                end
                else if(j >= 1 && j <= 8) begin
                    rx = byte[0];
                    byte = byte >> 1;
                end
                else begin
                    rx = 1;
                end
                @(posedge rs_clk);
            end
        end
        $fclose(fpr);
        #100000000;
        $display("Timeout");
        $finish;
    end
    always @ (tx) begin
        if(tx != 1'b1) begin
            $display("Find");
            #100000;
            $display("%x", dbg_header);
            $finish;
        end
    end
    /*
    always @ (posedge clk) begin
        $fwrite(fpw_rst, "%b\n", rst);
        $fwrite(fpw_rx, "%b\n", rx);
    end
    */
endmodule
