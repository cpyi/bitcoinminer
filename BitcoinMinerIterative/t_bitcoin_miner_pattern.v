`timescale 10ns/10ps
module t_bitcoin_miner();

    //Port definition
    reg clk, rst, rx, rs_clk;
    reg [7:0] byte;
    wire tx;
    wire [31:0] dbg_nonce;
    wire recv_ok;
    wire [511:0] recv_data;

    //Misc vars
    integer i, j, fp, r;

    //Module
    bitcoin_miner bm0(.clk(clk), .rst(rst), .rx(rx), .tx(tx), .dbg_nonce(dbg_nonce));
    pattern p0(.clk(clk), .rst(rst), .rx(tx), .recv_ok(recv_ok), .recv_data(recv_data));

    //Clock generation
    always #10 clk = ~clk;
    always #26040 rs_clk = ~rs_clk;
    //Dump file
    initial begin
        $dumpfile("bitcoin_miner_pattern.vcd");
        $dumpvars;
    end

    //Monitor
    initial begin
        $monitor(dbg_nonce);
    end

    initial begin

        //Open file
        fp = $fopen("bitcoin_miner.in", "r");

        //Initialization of registers
        clk = 0;
        rs_clk = 0;
        rst = 0;
        rx = 1;

        //Reset
        @(negedge clk);
        rst = 1;
        @(negedge clk);
        rst = 0;
        @(negedge clk);
        @(posedge rs_clk);
        //Loop
        for(i = 0;i < 76;i = i + 1) begin
            r = $fscanf(fp, "%2x", byte);
            //$display("%2x", byte);
            for(j = 0;j < 11;j = j + 1) begin
                if(j == 0) begin
                    rx = 0;
                end
                else if(j >= 1 && j <= 8) begin
                    rx = byte[0];
                    byte = byte >> 1;
                end
                else begin
                    rx = 1;
                end
                @(posedge rs_clk);
            end
        end
        $fclose(fp);
        #100000000;
        $display("Timeout");
        $finish;
    end
    always @ (recv_ok) begin
        if(recv_ok == 1'b1) begin
            $display("recv_data: %x", recv_data);
            #100;
            $finish;
        end
            
    end
    /*
    always @ (tx) begin
        if(tx != 1'b1) begin
            $display("Find");
            #100;
            $finish;
        end
    end
    */
endmodule
