module pattern(clk, rst, rx, recv_ok, recv_data);
    input clk;
    input rst;
    input rx;
    output reg recv_ok;
    wire rx_vld;
    wire [7:0] rx_data;
    wire tx;
    wire tx_rdy;
    reg [6:0] recv_cnt;
    output reg [511:0] recv_data;
    rs232 r0(.clk(clk), .rst(rst), .rx(rx), .tx_vld(1'b0), .tx_data(1'b0), .rx_vld(rx_vld), .rx_data(rx_data), .tx(tx), .tx_rdy(tx_rdy));
    initial begin
        recv_cnt = 0;
        recv_data = 0;
    end
    always @ (negedge clk) begin
        if(rx_vld) begin
            case(recv_cnt)
                0: recv_data[7:0] = rx_data;
                1: recv_data[15:8] = rx_data;
                2: recv_data[23:16] = rx_data;
                3: recv_data[31:24] = rx_data;
                4: recv_data[39:32] = rx_data;
                5: recv_data[47:40] = rx_data;
                6: recv_data[55:48] = rx_data;
                7: recv_data[63:56] = rx_data;
                8: recv_data[71:64] = rx_data;
                9: recv_data[79:72] = rx_data;
                10: recv_data[87:80] = rx_data;
                11: recv_data[95:88] = rx_data;
                12: recv_data[103:96] = rx_data;
                13: recv_data[111:104] = rx_data;
                14: recv_data[119:112] = rx_data;
                15: recv_data[127:120] = rx_data;
                16: recv_data[135:128] = rx_data;
                17: recv_data[143:136] = rx_data;
                18: recv_data[151:144] = rx_data;
                19: recv_data[159:152] = rx_data;
                20: recv_data[167:160] = rx_data;
                21: recv_data[175:168] = rx_data;
                22: recv_data[183:176] = rx_data;
                23: recv_data[191:184] = rx_data;
                24: recv_data[199:192] = rx_data;
                25: recv_data[207:200] = rx_data;
                26: recv_data[215:208] = rx_data;
                27: recv_data[223:216] = rx_data;
                28: recv_data[231:224] = rx_data;
                29: recv_data[239:232] = rx_data;
                30: recv_data[247:240] = rx_data;
                31: recv_data[255:248] = rx_data;
            endcase
            recv_cnt = recv_cnt + 1;
        end
    end
    always @ (*) begin
        if(recv_cnt == 32)
            recv_ok = 1'b1;
        else
            recv_ok = 1'b0;
    end
endmodule
